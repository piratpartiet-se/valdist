<?php
require("includes/open_database.php");
require("includes/header.php");
?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <?php
        $lan = $_GET["lan"];
        $kommun = $_GET["kommun"];

        $result = $conn->query("SELECT * FROM Län WHERE LänID=" . $conn->real_escape_string($lan));
        echo '<li class="breadcrumb-item"><a href="fget_kommuner.php?lan=' . $lan . '">' . $result->fetch_assoc()["Namn"] . '</a></li>';

        $result = $conn->query("SELECT * FROM Kommun WHERE KommunID=" . $conn->real_escape_string($kommun) . " AND LänID=" . $conn->real_escape_string($lan));
        $kommun_namn = $result->fetch_assoc()["Namn"];
        echo '<li class="breadcrumb-item active" aria-current="page">' . $kommun_namn . '</li>';
        ?>
      </ol>
    </nav>
  </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <h2><?php echo $_GET['typ'] == 'f' ? 'Förtidsröstningslokaler' : 'Vallokaler (rösta på valdagen)'; ?></h2>

    <?php
    $result = $conn->query('SELECT * from Kommun WHERE KommunID=' . $conn->real_escape_string($kommun) . ' AND länID=' . $conn->real_escape_string($lan));
    if ( $_GET['typ'] == 'f' ) {
        echo '<p>' . $kommun_namn . ' har ' . $result->fetch_assoc()["Röstb"] . ' röstberättigade. <a href="karta_obokade.html" target="_blank">Visa alla okbokade på karta</a></p>';
    } else {
        echo '<p>' . $kommun_namn . ' har ' . $result->fetch_assoc()["Röstb"] . ' röstberättigade. <a href="karta_obokade_valdag.html" target="_blank">Visa alla okbokade på karta</a></p>';
    }
    $kommun2siffrigt = $kommun < 10 ? "0$kommun" : "$kommun";
    $lan2siffrigt = $lan < 10 ? "0$lan" : "$lan";
    $oversiktskartelank = $_GET['typ'] == 'f' ? 'fortidsrosta' : 'vallokaler';

    ?>
  </div>
</div>
<!--
<div class="row" id="filter">
  <div class="col-md-8 offset-md-2">
    <div style="position: relative; float: right;"><a target="_blank" href="https://data.val.se/rostmottagning/<?php echo $oversiktskartelank?>?kommun=<?php echo $kommun2siffrigt;?>&lan=<?php echo $lan2siffrigt; ?>">> Öppna översiktskarta</a></div>
  -->
    <!--
    <a class="btn btn-info" href="fget_lokaler.php?typ=<?php echo $_GET['typ'] == 'f' ? 'v' : 'f'; ?>&status=<?php echo isset($_GET['status']) ? $_GET['status'] : ""; ?>&kommun=<?php echo $kommun; ?>&lan=<?php echo $lan; ?>" role="button">
      <i class="fas fa-caret-right"></i> Visa <?php echo $_GET['typ'] == 'f' ? 'vallokaler' : 'förtidsröstningslokaler'; ?> <i class="fas fa-filter"></i>
    </a>

    <?php if (isset($_GET['status']) && $_GET['status'] == 'obokade' ) { ?>
    <a class="btn btn-info" href="fget_lokaler.php?typ=<?php echo $_GET['typ']; ?>&kommun=<?php echo $kommun; ?>&lan=<?php echo $lan; ?>" role="button"><i class="fas fa-caret-right"></i>Visa även bokade <i class="fas fa-filter"></i></a>
    <?php } else { ?>
    <a class="btn btn-info" href="fget_lokaler.php?typ=<?php echo $_GET['typ']; ?>&status=obokade&kommun=<?php echo $kommun; ?>&lan=<?php echo $lan; ?>" role="button"><i class="fas fa-caret-right"></i>Dölj bokade <i class="fas fa-filter"></i></a>
    <?php } ?>
  -->
<!--
  </div>
</div>
-->
<?php
$inh_status = isset($_GET['status']) ? $_GET['status'] : '';
$f_lokal_lank = 'fget_lokaler.php?typ=f&status='.$inh_status.'&kommun='.$kommun.'&lan='.$lan;
$v_lokal_lank = 'fget_lokaler.php?typ=v&status='.$inh_status.'&kommun='.$kommun.'&lan='.$lan;
$valmyndighetens_karta = 'https://data.val.se/rostmottagning/'.$oversiktskartelank.'?kommun='.$kommun2siffrigt.'&lan='.$lan2siffrigt;
?>

<div class="row mb-2" id="tab-container">
  <div class="col-md-8 offset-md-2">
      <ul class="nav nav-tabs nav-justified">
        <li class="nav-item">
          <a class="nav-link<?php echo $_GET['typ'] === 'f' ? ' active': ''; ?>" href="<?php echo $f_lokal_lank; ?>">Förtidsröstningslokaler</a>
        </li>
        <li class="nav-item mr-5">
          <a class="nav-link<?php echo $_GET['typ'] === 'v' ? ' active': ''; ?>" href="<?php echo $v_lokal_lank; ?>">Vallokaler (Valdagsröstning)</a>
        </li>
        <li class="nav-item mb-1 mr-1">
        <?php if (isset($_GET['status']) && $_GET['status'] == 'obokade' ) { ?>
        <a class="btn btn-info nav-link" href="fget_lokaler.php?typ=<?php echo $_GET['typ']; ?>&kommun=<?php echo $kommun; ?>&lan=<?php echo $lan; ?>" role="button"><i class="fas fa-caret-right"></i> Visa även bokade lokaler <i class="fas fa-filter"></i></a>
        <?php } else { ?>
        <a class="btn btn-info nav-link" href="fget_lokaler.php?typ=<?php echo $_GET['typ']; ?>&status=obokade&kommun=<?php echo $kommun; ?>&lan=<?php echo $lan; ?>" role="button"><i class="fas fa-caret-right"></i> Dölj bokade lokaler <i class="fas fa-filter"></i></a>
        <?php } ?>
        </li>
        <li class="nav-item mr-1">
          <a target="_blank" class="btn btn-primary nav-link" href="<?php echo $valmyndighetens_karta; ?>" role="button"><i class="fas fa-caret-right"></i> Öppna översiktskarta (via val.se) <i class="fas fa-map"></i></a>
        </li>
      </ul>
      <ul  class="nav nav-tabs">

      </ul>
  </div>
</div>

<main class="row">
  <div class="col-md-8 offset-md-2">

    <?php
    //echo $sql . '<br>';
    $obokade = isset($_GET['status']) && $_GET['status'] == 'obokade' ? ' BookingID IS NULL AND ' : '';
    $result = $conn->query("SELECT vallokal.* FROM vallokal LEFT JOIN Booking ON vallokal.LokalKod = Booking.LokalID WHERE $obokade KommunKod=" .  $conn->real_escape_string($kommun) . " AND LanKod=" .  $conn->real_escape_string($lan). " AND Typ='".($_GET['typ'] == 'f' ? 'F' : 'V')."' ORDER BY lokal" );
    if ($result->num_rows > 0) {
        echo '<table class="table table-bordered table-hover">';
        echo '<thead><tr><th>Status <i class="fas fa-clipboard-check"></i></th><th>Namn på lokal <i class="fas fa-tag"></i></th><th>Kort adress <i class="fas fa-map-pin"></i></th><th>Röstberättigade</th><th>Karta <i class="fas fa-map"></th></tr></thead><tbody>';

        // output data of each row
        while($row = $result->fetch_assoc()) {

            if ($row["Status"] == "K") {
                echo '<tr><td class="table-success">Klar</td>';
            } else if ($row["Status"] == "B") {
                echo '<tr><td class="table-warning">Bokad</td>';
            } else {
                echo '<tr><td class="table-danger">Ej bokad</td>';
            }
            echo  '<td><a href="fget_lokal.php?lokal=' . $row["LokalKod"] . '">' . $row["lokal"] . '</a></td>';

            echo '<td>' . ($row["Adress2"] == '' ? '??' : $row["Adress2"]) . ($row["Postort"] == '' ? '' : ', ' . $row["Postort"]) . ($row["Adress1"] == '' ? '' : ' | ' . $row["Adress1"]) . '</td>';
            echo '<td>'.htmlspecialchars($row['VoterCount']).'</td>';

            echo '<td><a target="_blank" class="btn btn-primary" href="https://www.google.com/maps/search/?api=1&query=' . $row["Lat"] . ',' . $row["Lng"] . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Öppna karta <i class="fas fa-map"></i></a></td></tr>';
        }
        echo '</tbody></table>';
    } else {
        echo '<div class="alert alert-warning" role="alert">Vi hittade tyvärr inga lokaler i vår databas. Vänligen kontakta ansvarig.</div>';
    }

    ?>

  </div>
</main>

<?php require("includes/footer.php");
