<?php
require("includes/open_database.php");

$lokalkod = $_GET['lokal'];
$lokal_data = get_lokal($conn, $lokalkod);
$bokning = get_booking_by_lokalkod($conn, $lokalkod);

if ($lokal_data !== null && $bokning !== null && $bokning['UserID'] === $GLOBAL_USERID && check_if_lokal_has_received_sedlar($conn, $lokalkod) === false){
    $query = 'UPDATE booking SET AntalR='.$lokal_data['AntalR'].', AntalL='.$lokal_data['AntL'].', AntalK='.$lokal_data['AntalK'].' WHERE LokalID='.$lokalkod.' AND UserID='.$GLOBAL_USERID;
    $conn->query($query);
}

header('Location: mina_lokaler.php');