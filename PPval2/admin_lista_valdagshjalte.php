<?php
require("includes/open_database.php");

set_time_limit(7200);
ini_set('max_execution_time',7200);


if (!$isadmin) {
	header("Location: /");
	exit;
}
require("includes/header.php");

echo '<main class="row">  <div class="col-md-8 offset-md-2">';


$result = $conn->query("select User.*, Kommun.Namn kommun, Kommun.ID as kommunid from valdagshjalte left join User on valdagshjalte.userid = User.UserID left join Kommun on valdagshjalte.kommunid = Kommun.ID order by valdagshjalte.id;");
echo '<table class="table table-bordered table-hover">';

while($row = $result->fetch_assoc()) {
	$res2 = $conn->query(" select User.* from kommunansvarig left join User on kommunansvarig.userid = User.UserID where kommunid = $row[kommunid] limit 1;");
	$kommunansvarig = '-';
	if ($res2->num_rows > 0 ) {
		$row2 = $res2->fetch_assoc();
		if($row2['UserID'] == $row['UserID']) {
			$kommunansvarig = 'Är kommunansvarig';
		} else {
			$kommunansvarig = htmlspecialchars($row2['namn'] . ' ' . $row2['efternamn'] . ' ' . $row2['telefon']);
		}
	}
	echo '<tr><td>'.$row['kommun'].'</td><td>'.$row['UserID'].'</td><td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td><td>' . htmlspecialchars($row['adress'] . ' '  . $row['postadress']) . '</td><td>' . htmlspecialchars($row['mail'])  . '</td><td>' . htmlspecialchars($row['telefon']) . '</td><td>'.$kommunansvarig.'</td></tr>';
}

echo '</table>';

echo '</div></main>';
