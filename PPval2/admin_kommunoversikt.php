<?php
require("includes/open_database.php");

if (!$isadmin) {
        header("Location: /");
	exit;
}

require("includes/header.php");

$lan = isset($_GET['lan']) ? $conn->real_escape_string($_GET['lan']) : die();
$kommun = isset($_GET['kommun']) ? $conn->real_escape_string($_GET['kommun']) : die();
$sql = 'select vallokal.lokal, vallokal.Typ, vallokal.Status, vallokal.VoterCount, concat(User.namn, " ", User.efternamn) namn, User.adress, User.postadress, User.telefon, User.mail, vallokal.LokalKod as _LokalKod from vallokal left join Booking on vallokal.LokalKod = Booking.LokalID left join User on Booking.UserID = User.UserID where LanKod = "'.$lan.'" and KommunKod = "'.$kommun.'" order by Typ, lokal';


$result = $conn->query('select Namn from Kommun where LänID = "'.$lan.'" and KommunID = "'.$kommun.'"');
$kommunnamn = $result->fetch_assoc()['Namn'];


echo '<div class="row">
  <div class="col-md-8 offset-md-2">';

$classes = ['' => 'table-danger', 'O' => 'table-danger', 'K' => 'table-success', 'B' => 'table-warning'];

?>

<script>
function markera_som_klar(lokalkod) {
	console.log(this);
	if(!confirm('Markera som klar?')) {
		return false;
	}

	$.get('lokal_klar.php?lokal=' + lokalkod).done(function(res) {
		$('td[kod="'+lokalkod+'"').parent().removeClass().addClass('table-success');
	});
	return false;
}
</script>

<?php

echo '<h1>Översikt över lokaler och utläggare i '.$kommunnamn.'</h1>';
echo '<table class="table table-bordered table-hover">';
echo '<thead><tr>
		<th>Lokal</th>
		<th>Typ</th>
		<th>Status</th>
		<th>Röstberättigade</th>
		<th>Utläggare</th>
		<th>Address</th>
		<th>Postort</th>
		<th>Telefon</th>
		<th>Mail</th>
	</tr></thead>';
$result = $conn->query($sql);

while($row = $result->fetch_assoc()) {
	$i = 0;
	if($row['Status'] == '') {
		$row['Status'] = 'O';
	}
	echo "<tr class='" . $classes[$row['Status']] ."'>";
	foreach ($row as $key => $val) {
		if ($key[0] == '_' ) {
			continue;
		} else if ($key == 'Status') {
			$trans = ['O' => 'Obokad', 'K' => 'Klar', 'B' => 'Bokad'];
			echo "<td kod='$row[_LokalKod]'><a href='#' onclick='return markera_som_klar(\"$row[_LokalKod]\");'>".$trans[$row['Status']]."</a></td>";
		} else if ($key == 'lokal') {
			echo "<td><a href='fget_lokal.php?lokal=$row[_LokalKod]'>$row[lokal]</a></td>";
		} else {
			echo "<td>$val</td>";
		}
		$i++;
	}
	echo '</tr>';
}
echo '</div></div>';

require("includes/footer.php");
