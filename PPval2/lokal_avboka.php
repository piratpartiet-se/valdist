<?php
require("includes/open_database.php");

$lokalkod = $_GET['lokal'];
$lokal_data = get_lokal($conn, $lokalkod);
$bokning = get_booking_by_lokalkod($conn, $lokalkod);

if ($lokal_data !== null && $bokning !== null && $bokning['UserID'] === $GLOBAL_USERID){
	$conn->query('UPDATE vallokal SET Status = "O" WHERE LokalKod="' . $conn->real_escape_string($lokalkod) . '"');
	$conn->query('DELETE FROM Booking WHERE UserID=' . $conn->real_escape_string($GLOBAL_USERID) . ' AND LokalID="' . $conn->real_escape_string($lokalkod) . '"');
}
header('Location: mina_lokaler.php');
