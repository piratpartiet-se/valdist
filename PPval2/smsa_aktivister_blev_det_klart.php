<?php
require("includes/open_database.php");

set_time_limit(7200);
ini_set('max_execution_time',7200);


if (!$isadmin) {
	header("Location: /");
	exit;
}

require("includes/header.php");

echo '<main class="row">  <div class="col-md-8 offset-md-2">';


if(isset($_GET['done']) && $_GET['done'] == 1) {
	echo '<h1>Klart!</h1>';
}

//$result = $conn->query("SELECT concat(namn, ' ', efternamn) namn, concat(adress, ' ', postadress) address, mail, telefon FROM User WHERE UserID IN (select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod  = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID GROUP BY kommunansvarig.UserID);");
//$result = $conn->query("select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID;");

//$result = $conn->query("select KommunKod, LanKod from Booking LEFT JOIN vallokal ON LokalID = LokalKod WHERE Booking.AntalR = 0 group by KommunKod, LanKod;");

$result = $conn->query("select * from User;");
$numbers = [];
echo '<table class="table table-bordered table-hover">';

while($row = $result->fetch_assoc()) {
//	$result2 = $conn->query("SELECT * FROM Booking WHERE Booking.LokalID IN (select LokalKod from Kommun RIGHT JOIN vallokal ON KommunID = KommunKod AND LänID = LanKod WHERE Kommun.ID = $row[kommunid]) and Booking.UserID != $row[userid] AND Booking.AntalR = 0;");
//	while($row2 = $result2->fetch_assoc()) {
//		$result3 = $conn->query("SELECT * FROM User WHERE UserID = $row[userid]");
//		while($row3 = $result3->fetch_assoc()) {
			$numbers[$row['telefon']] = $row['telefon'];
			echo '<tr><td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td><td>' . htmlspecialchars($row['adress'] . ' '  . $row['postadress']) . '</td><td>' . htmlspecialchars($row['mail'])  . '</td><td>' . htmlspecialchars($row['telefon']) . '</td></tr>';
//		}
//	}
}

echo '</table>';

echo 'Totalt kommer ' . count($numbers) . ' SMS gå ut. ';
echo '<a href="smsa_aktivister_blev_det_klart.php?send=1">Skicka</a>';

echo '</div></main>';

if(isset($_GET['send']) && $_GET['send'] == 1) {
	foreach($numbers as $i => $number) {
//		file_put_contents($log, "$i: $number\n", FILE_APPEND);
		echo "$i: $number<br>";
		$sms = array(
		  "from" => "PP",   /* Can be up to 11 alphanumeric characters */
		  "to" => $number,  /* The mobile number you want to send to */
		  "message" => "Piratpartiet: Hej! Idag har förtidsröstningen börjat! Om du har skrivit upp dig på att hjälpa till med att lägga ut valsedlar en förtidsröstningslokal, hur har det gått? Om det gått bra, gå in på https://valsedel.piratpartiet.se/mina_lokaler.php#F Under varje lokal där så klicka på klart-knappen. Har något strulat eller det inte kommit ut valsedlar? Maila johan.karlsson@piratpartiet.se eller smsa Johan på 0737689268 så löser vi problemen." ,
		);
		sleep(1);
		$res = sendSMS($sms);
		echo $res;
		flush();
	}
//	header("Location: smsa_kommunansvariga.php?done=1");

}

exit;
