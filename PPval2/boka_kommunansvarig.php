<?php
require("includes/open_database.php");

if($GLOBAL_USERID > 0) {
    $result = $conn->query('SELECT * FROM User where userid=' . (int)$GLOBAL_USERID);
    if ($result->num_rows > 0) {
	     boka_kommunansvarig($conn, (int)$GLOBAL_USERID, (int)$_GET['lan'], (int)$_GET["kommun"]);
    }
    exit;
}

header("Location: registrera.php?action=kommunansvarig&kommun=".(int)$_GET["kommun"]."&lan=".(int)$_GET['lan']);
exit;
