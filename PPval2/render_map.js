class ValMap {
  constructor(target, accessToken, startingCoords=null, startZoom=null){
    var that = this;
    this.target = target
    this.token = accessToken
    this.map = null
    this.markers = new Object()
    this.startingCoords = [18, 59.3]
    if (startingCoords === null){
      console.log('try auto');
      if (navigator.geolocation) {
        console.log('auto enabled');
        navigator.geolocation.getCurrentPosition(function(data){that.center_current_position(data,that)});
      }
    } else {
      //console.log('from input obj');
      this.startingCoords = [startingCoords.lat, startingCoords.long]
      //this.startingCoords = (startingCoords !== null) ? [startingCoords.lat, startingCoords.long] : [18, 59.3]
    }

    this.startZoom = (startZoom !== null) ? startZoom : 9


    this.ok = this.check()
    this.init()
  }
  center_current_position(position, that){
    console.log('trying to auto')
    console.log(position)
    if (that.map === undefined ){
      console.log("autoset start to location")
      that.startingCoords[position.coords.latitude, position.coords.longitude]
    } else {
      console.log("jump")
      that.map.jumpTo({center: [position.coords.longitude, position.coords.latitude]})
    }
  }
  check(){
    if(window.jQuery && window.mapboxgl){
      return true;
    }
    return false;
  }
  init(){
    this.map = null
    $('#'+this.target).empty() // Tom jävla canvas utifall nån tjomme gjort FEL
    this.map = new mapboxgl.Map({
      accessToken: this.token,
      container: this.target,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.startingCoords,
      zoom: 9,
      projection: 'globe'
    });
    this.map.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
        enableHighAccuracy: true
        },
        // When active the map will receive updates to the device's location as it changes.
        trackUserLocation: true,
        // Draw an arrow next to the location dot to indicate which direction the device is heading.
        showUserHeading: true
      })
    );
  }
  add_marker(obj){
    let marker = new mapboxgl.Marker()
    .setLngLat([obj.long, obj.lat])
    .setDraggable(false)
    .setPopup(new mapboxgl.Popup().setHTML(obj.html))
    if (this.markers[obj.id] !== null && this.markers[obj.id] !== undefined){
      // Om markern redan finns så plockar vi bort gamla innan vi dammar dit en ny
      this.delete_marker(obj.id)
    }
    this.markers[obj.id] = marker
    marker.addTo(this.map);
  }

  delete_marker(key){
    console.log('Deleting marker with key ' + key)
    if(this.markers[key] !== null && this.markers[key] !== undefined){
      this.markers[key].remove()
      delete this.markers[key];
    }
  }
}

const MapDataType = Object.freeze({
  Fortidsrostningslokaler: 'Fortidsrostningslokaler',
  Valdagslokaler: 'Valdagslokaler',
  MinaBokade: 'MinaBokade',
  MinaAvklarade: 'MinaAvklarade',
  MinaEjAvklarade: 'MinaEjAvklarade',
  OppnaIdag: 'OppnaIdag',
  OppnaIdagOchImorgon: 'OppnaIdagOchImorgon',
  Undefined: 'Undefined'
  // Iterera över fanskapet med Object.keys(MapDataType).forEach(dt => console.log("DataType:", dt))
});

function validateMapDataType(s){
  return (MapDataType[s] === s);
}

class DataFormatter{
  constructor(data_type){
    this.type = MapDataType.Undefined
    if (validateMapDataType(data_type) === true){
      this.type = data_type
    }
  }
  parse(data){
    switch(this) {
      case MapDataType.Fortidsrostningslokaler:
        data = this.parseFortidsrostningslokaler(data)
        break;
      case y:
        // code block
        break;
      default:
        data = parseDefault(data)
    }
    return data;
  }
  parseDefault(data){
    let r = {
      id: data.LokalKod,
      lat: data.Lat,
      long: data.Lng,
      html: '<div><p>' + data.lokal + '</p><p>' + data.Adress1 + '</p><p>' + data.Adress2 + '</p></div'
    }
    return r
  }
  parseFortidsrostningslokaler(data){
    let r = this.parseDefault(data)
    // Här kan du leka bäst fan du vill
    r.html = '<div><p>veve jquery som en häst</p></div>'
    return r
  }
}


class CompleteMap{
  constructor(target, api_key, map_data_type, source, coords=null, zoom=null){
    this.target = target
    this.api_key = api_key
    this.type = map_data_type
    this.source = source

    this.map = new ValMap(target, api_key, coords, zoom)

    this.valid_source = validateMapDataType(this.source)
    this.dataLoader = new DataLoader(this.source, this.type)
    this.data = null
    this.formatter = new DataFormatter(this.type)

    this.dataLoader.data.then((data) => {
      console.log('Remote Data Loaded!');
      console.log('Fetched ' + data.length + ' rows!')
      this.data = data
      this.process()
    })
    .catch((error) =>{
      console.error(error)
      this.data = []
    })
  }
  process(){
    this.data.forEach(d => {
      this.map.add_marker(this.formatter.parseDefault(d))
    });
  }
}

class DataLoader{
  constructor(source, action=null){
    this.uri = source
    if (action !== null){
      this.params = {'action': action}
    } else {
      this.params = {}
    }
    this.data = new Promise((resolve, reject) => {
      $.ajax({
        url: this.uri,
        type: 'GET',
        data: this.params,
        success: function (data) {
          resolve(data)
        },
        error: function (error) {
          reject(error)
        },
      })
    });
  }
}

console.log('Loaded render_map.js')
