<?php
require("includes/open_database.php");
require("includes/header.php");
?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <?php
        $lannr = $_GET["lan"];
        $result = $conn->query("SELECT * FROM Län WHERE LänID=" . $conn->real_escape_string($lannr));
        echo '<li class="breadcrumb-item active" aria-current="page">' . $result->fetch_assoc()["Namn"] . '</li>';
        ?>
      </ol>
    </nav>
  </div>
</div>
<main class="row">
  <div class="col-md-8 offset-md-2">
    <?php
    //$result = $conn->query("SELECT * FROM Kommun WHERE LänID=" . $conn->real_escape_string($_GET["lan"]) . " ORDER BY Namn");
    $result = $conn->query("select *, ROUND((1-(obokade/total))*100, 2) as tackningsgrad from (select kommunkod, LanKod, sum(total) total, sum(obokade) obokade, sum(bokade) bokade, sum(klara) klara from (select KommunKod, LanKod, LokalKod, VoterCount as total, if(Status=null or(lower(Status)='o') or(Status=''), VoterCount, 0) as obokade, if(lower(Status)='b', VoterCount, 0) as bokade, if(lower(Status)='k', VoterCount, 0) as klara from vallokal) d group by LanKod, KommunKod) d left join Kommun on Kommun.KommunID=d.kommunkod and Kommun.LänID=d.LanKod where LanKod=".$conn->real_escape_string($_GET["lan"])." order by Namn");
    if ($result->num_rows > 0) {
        echo '<table class="table table-bordered table-hover">';
        echo '<thead><tr><th>Kommun <i class="fas fa-globe-africa"></i></th>';
	      /* <th>Lokaler <i class="fas fa-city"></i></th> */
	      echo '<th>Ca. antal röstberättigade <i class="fas fa-check"></i></th><th>Täckningsgrad</th><th>Täckning förtid</th></tr></thead><tbody>';
        // output data of each row
        while($row = $result->fetch_assoc()) {
	        $ansvarigresult = $conn->query("SELECT COUNT(*) hasAnsvarig FROM kommunansvarig WHERE kommunid = $row[ID]");
		$tackning_ft_result = $conn->query("select round(100 * bokade/totalt,1) procent from (select sum(vallokal.AntalR) totalt, SUM(IF(Booking.BookingID IS NULL, 0, vallokal.AntalR)) bokade FROM vallokal LEFT JOIN Booking ON vallokal.LokalKod = Booking.LokalID WHERE vallokal.LanKod = $row[LanKod] and vallokal.KommunKod = $row[KommunKod] AND vallokal.Typ = 'F') t1;");
		$tackning_ft = $tackning_ft_result->fetch_assoc()['procent'];
	        $ansvarig = $ansvarigresult->fetch_assoc();
	        $ansvarig = $ansvarig['hasAnsvarig'] > 0 ? 1  : 0;
          	echo "<tr class='kommunrow' data-kommunid='$row[KommunID]' data-haskommunansvarig='$ansvarig' data-lanid='$lannr'><td class='kommunnamn'><a href='get_lokaler.php?typ=v&kommun=$row[KommunID]&lan=$row[LänID]' onclick='return false;'>$row[Namn]</a></td>";
          	echo '<td>' . $row["Röstb"] . '</td>';

			//$display_val = str_ends_with((string)$row['tackningsgrad'], '.00') ? substr($row['tackningsgrad'], 0, strlen($row['tackningsgrad'])-3) : $row['tackningsgrad'];
			if ( $row['tackningsgrad'] == 100 ) {
				$tackning_ft = 100;
			}
			echo '<td>'.htmlspecialchars($row['tackningsgrad']).'%</td>';
			echo '<td>'.htmlspecialchars($tackning_ft).'%</td>';
			echo '</tr>';
        }
        echo '</tbody></table>';
    } else {
        echo '<div class="alert alert-warning" role="alert">Vi hittade tyvärr inga kommuner i vår databas. Vänligen kontakta ansvarig.</div>';
    }
    $conn->close();
    ?>
  </div>
</main>

<div style="display: none;" id="kommunpopup" title="Hur vill du hjälpa?">
	<p><strong>Hur vill du hjälpa <span id="kommunnamn"></span>?</strong> Du kan välja flera alternativ.
	<p id="p-kommunansvarig">
		<strong>Kommunansvarig</strong><br>
		Som kommunansvarig får du valsedlar skickade direkt till dig. Du lämnar sedan vidare valsedlar till andra aktivister i kommunen. Vi behöver en kommunansvarig per kommun.<br>
		<button id="kommunansvarigbutton">Mer info</button>
	</p>
	<p>
		<strong>Valdagshjälte</strong><br>
		På valdagen (och när förtidsröstningen börjar) så hjälper du till att täcka in alla vallokaler som ingen annan tagit.<br>
		<button id="valdagshjaltebutton">Mer info</button>
	</p>
	<p>
		<strong>Lägga ut valsedlar i en eller flera lokaler</strong><br>
		Du får valsedlar av kommunansvarig och lägger ut dom i en eller flera vallokaler.<br>
		<button id="laggavalsedlarbutton">Välj lokaler</button>
	</p>
</div>

<script>
	$('.kommunrow').on('click', function(ev) {
		$(this).data('haskommunansvarig') == '1' ? $('#p-kommunansvarig').hide() : $('#p-kommunansvarig').show();

		$("#kommunnamn").text($(this).find('.kommunnamn').text());
		$("#kommunpopup").find('button').data('kommunid', $(this).data('kommunid'));
		$("#kommunpopup").find('button').data('lanid', $(this).data('lanid'));
		$( "#kommunpopup" ).dialog({ modal: true });
	});

	$('#kommunpopup button').on('click', function(ev) {
		if( $(this).attr('id') == 'laggavalsedlarbutton' ) {
			window.location.href = "fget_lokaler.php?typ=v&kommun="+$(this).data('kommunid')+"&lan="+$(this).data('lanid');
		}
		if( $(this).attr('id') == 'valdagshjaltebutton' ) {
			window.location.href = "valdagshjalte.php?kommun="+$(this).data('kommunid')+"&lan="+$(this).data('lanid');
		}
		if( $(this).attr('id') == 'kommunansvarigbutton' ) {
			window.location.href = "kommunansvarig.php?kommun="+$(this).data('kommunid')+"&lan="+$(this).data('lanid');
		}
	});

</script>

<?php require("includes/footer.php"); ?>
