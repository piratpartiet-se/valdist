<?php
require("includes/open_database.php");

if (!$isadmin) {
        header("Location: /");
	exit;
}

require("includes/header.php");

$sql = "select User.UserID, namn, efternamn, adress, postadress, telefon, mail from kommunansvarig left join User on kommunansvarig.userid = User.UserID and coalesce(sent, 0) = 0 WHERE User.UserID IS NOT NULL group by User.UserID order by User.UserID;";


echo '<div class="row">
  <div class="col-md-8 offset-md-2">';

echo '<h1>Valsedlar som behöver packas</h1>';
echo '<form action="save_valsedelspack.php" method="post">';
echo '<table class="table table-bordered table-hover">';
echo '<thead><tr>
		<th>Namn</th>
		<th>Address</th>
		<th>Postnr</th>
		<th>Postort</th>
		<th>Telefon</th>
		<th>Mail</th>
		<th>Antal</th>
		<th>Skickat</th>
	</tr></thead>';

$result = $conn->query($sql);

while($row = $result->fetch_assoc()) {
        echo '<tr>';
        echo '<td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td>';
        echo '<td>' . htmlspecialchars($row['adress']) . '</td>';
        $postnr = '';
	$postort = '';
        foreach(str_split($row['postadress']) as $char) {
		if (is_numeric($char)) {
			$postnr .= $char;
		} else {
			$postort .= $char;
		}
	}
	$postort = trim($postort);
	$postnr = trim($postnr);

	$sql = "select round(1.1*Sum(AntalR)) AntalR, round(1.1*SUM(AntL)) AntalL, round(1.1*SUM(AntalK)) AntalK, Kommun.Namn kommun, Län.Namn lan from Kommun right join vallokal on Kommun.KommunID = vallokal.KommunKod and Kommun.LänID = vallokal.LanKod right join Län on Kommun.LänID = Län.LänID WHERE  ID in (select kommunid from kommunansvarig where coalesce(sent, 0) = 0 and kommunansvarig.userid = $row[UserID]) and vallokal.lokal is not null group by Kommun.ID;";

	$result2 = $conn->query($sql);
	$riks = 0;
	$landsting = [];
	$kommun = [];
	$p = '';
        while($row2 = $result2->fetch_assoc()) {
		$riks += $row2['AntalR'];

		if(!isset($landsting[$row2['lan']])) {
			$landsting[$row2['lan']] = 0;
		}
		$landsting[$row2['lan']] += $row2['AntalL'];

		if(!isset($kommun[$row2['kommun']])) {
			$kommun[$row2['kommun']] = 0;
		}
		$kommun[$row2['kommun']] += $row2['AntalK'];

		$p = "Riks: $riks<br>";
		foreach($landsting AS $k => $v) {
			if($v == 0) {
				continue;
			}
			$p .= "$k: $v<br>";
		}
		foreach($kommun AS $k => $v) {
			if($v == 0) {
				continue;
			}
			$p .= "$k: $v<br>";
		}

	}
        echo '<td>' . htmlspecialchars($postnr) . '</td>';
        echo '<td>' . htmlspecialchars($postort) . '</td>';
        echo '<td>' . htmlspecialchars($row['telefon']) . '</td>';
        echo '<td>' . htmlspecialchars($row['mail']) . '</td>';
        echo '<td>'.$p.'</td>';
        echo '<td><input type="checkbox" name="userid-'.$row['UserID'].'"</td>';
	echo '</tr>';

}
echo '</table>';
echo '<button type="submit">Skicka</button>';
echo '</div></div>';

require("includes/footer.php");
