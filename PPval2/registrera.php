<?php
require("includes/open_database.php");
require("includes/header.php");

$lokalkod = $conn->real_escape_string(isset($_GET["lokalkod"]) ? $_GET["lokalkod"] : '');
$kommun = $conn->real_escape_string(isset($_GET["kommun"]) ? $_GET["kommun"] : '');
$lan = $conn->real_escape_string(isset($_GET["lan"]) ? $_GET["lan"] : '');

$result = $conn->query('SELECT * FROM vallokal where LokalKod="' . $lokalkod . '"');
$lokal = $result->fetch_assoc();

$first_name = $last_name = $street_adress = $post_address = $phone_number = $email_address = '';

if($GLOBAL_USERID > 0) {
    $result = $conn->query('SELECT * FROM User where userid=' . (int)$GLOBAL_USERID);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $first_name = $row["namn"];
            $last_name = $row["efternamn"];
            $street_adress = $row["adress"];
            $post_address = $row["postadress"];
            $phone_number = $row["telefon"];
            $email_address = $row["mail"];
        }
    }
}
?>

<main class="row">
  <div class="col-md-8 offset-md-2">
    <h2>Dina personuppgifter</h2>
    <p>Dina uppgifter behövs så vi kan kontakta dig för att ge dig valsedlar</p>
    <hr>

    <form action="registrera_save.php" method="post">
      <input type="hidden" name="action" value="<?php echo isset($_GET['action']) ? $_GET['action'] : ''; ?>">
      <input type="hidden" name="lokalkod" value="<?php echo $lokalkod == '' ? '' : $lokalkod; ?>">
      <input type="hidden" name="kommun" value="<?php echo $kommun == '' ? '' : $kommun; ?>">
      <input type="hidden" name="lan" value="<?php echo $lan == '' ? '' : $lan; ?>">

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="first-name">Förnamn</label>
          <input type="text" class="form-control" id="first-name" name="first_name" placeholder="" value="<?php echo htmlspecialchars($first_name) == '' ? '' : $first_name; ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="last-name">Efternamn</label>
          <input type="text" class="form-control" id="last-name" name="last_name" placeholder="" value="<?php echo htmlspecialchars($last_name) == '' ? '' : $last_name; ?>">
        </div>
      </div>
      <div class="form-group">
        <label for="street-address">Gatuaddress</label>
        <input type="text" class="form-control" id="street-address" name="street_address" placeholder="" value="<?php echo htmlspecialchars($street_adress) == '' ? '' : $street_adress; ?>">
      </div>
      <div class="form-group">
        <label for="post-address">Postnummer och Postort</label>
        <input type="text" class="form-control" id="post-address" name="post_address" placeholder="" value="<?php echo htmlspecialchars($post_address) == '' ? '' : $post_address; ?>">
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="email-address">E-postadress</label>
          <input type="email" class="form-control" id="email-address" name="email_address" placeholder="" value="<?php echo htmlspecialchars($email_address) == '' ? '' : $email_address; ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="phone-number">Mobilnummer</label>
          <input type="tel" class="form-control" id="phone-number" name="phone_number" placeholder="" value="<?php echo (htmlspecialchars($phone_number) == '' ? '' : $phone_number); ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="password">Lösenord</label>
          <input type="password" class="form-control" id="password" name="password" placeholder="" value="">
        </div>
        <div class="form-group col-md-6">
          <label for="password">Bekräfta Lösenord</label>
          <input type="password" class="form-control" id="password-confirmation" name="password_confirmation" placeholder="" value="">
        </div>
      </div>
      <hr>

      <button id="registerBtn" type="submit" class="btn btn-primary"><i class="fas fa-caret-right"></i> Gå vidare <i class="fas fa-clipboard-list"></i></button>
    </form>
  </div>
</main>
<script>

	$('#registerBtn').on('click', function(ev) {
		if ( $('#last-name').val().length < 3 ) {
			alert('Fyll i efternamn');
			return false;
		}

		if ( $('#first-name').val().length < 3) {
			alert('Fyll i förnamn');
			return false;
		}

		if ( $('#street-address').val().length < 6 ) {
			alert('Fyll i gatuaddress');
			return false;
		}

		if ( $('#post-address').val().length < 9 ) {
			alert('Fyll i postnummer och ort');
			return false;
		}

		if ( $('#phone-number').val().length < 10 ) {
			alert('Fyll i ditt mobilnummer');
			return false;
		}

    if ( $('#password').val().length < 8 ) {
      alert('Lösenordet måste vara minst 8 tecken');
      return false;
    }

    if ($('#password').val() !== $('#password-confirmation').val()) {
      alert('Lösenorden stämmer inte');
      return false;
    }


		var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		var mail = $('#email-address').val();
		var res = re.test(mail);

		if ( ! res ) {
			alert('Fyll i din mailaddress');
			return false;
		}

	});

</script>
