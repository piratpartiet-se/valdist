<?php
require("includes/open_database.php");
require("includes/header.php");
?>

<div class="row">
  <div class="col-md-8 offset-md-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <?php


        $lokalkod = $_GET["lokal"];

        $result = $conn->query("SELECT * FROM vallokal WHERE LokalKod='" . $conn->real_escape_string($lokalkod) . "'");

        $row = $result->fetch_assoc();
        $lan = $row["LanKod"];
        $kommun = $row["KommunKod"];

        $result = $conn->query("SELECT * FROM Län WHERE LänID='" . $lan . "'");
        echo '<li class="breadcrumb-item"><a href="fget_kommuner.php?lan=' . $lan . '">' . $result->fetch_assoc()["Namn"] . '</a></li>';

        $result = $conn->query("SELECT * FROM Kommun WHERE KommunID='" . $kommun . "' AND LänID='" . $lan . "'");
        $kommun_row = $result->fetch_assoc();
        $kommun_namn = $kommun_row["Namn"];

        if ($row["Typ"] == "F") {
            echo '<li class="breadcrumb-item"><a href="fget_lokaler.php?typ=f&kommun=' . $kommun . '&lan=' . $lan . '">' . $kommun_namn . ' (endast förtidslokaler)</a></li>';
        } else {
            echo '<li class="breadcrumb-item"><a href="fget_lokaler.php?typ=v&kommun=' . $kommun . '&lan=' . $lan . '">' . $kommun_namn . ' (endast valdagslokaler)</a></li>';
        }

        $result = $conn->query("SELECT * FROM vallokal WHERE LokalKod='" . $conn->real_escape_string($lokalkod) . "'");
        $lokal_namn = $result->fetch_assoc()["lokal"];

        echo '<li class="breadcrumb-item active" aria-current="page">' . $lokal_namn . '</li>';
        ?>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-8 offset-md-2">

    <?php

    $done = $booked = $booked_by_me = false;

    // Hämta status för vallokalen (klar, bokad, obokad)
    $result = $conn->query('SELECT * FROM vallokal where LokalKod="' . $conn->real_escape_string($lokalkod) . '"');
    $lokal = $result->fetch_assoc();

    // Hämta bokningar för denna lokal
    $bookings = $conn->query('SELECT * FROM Booking where LokalID="' .  $conn->real_escape_string($lokalkod) . '"');
    if ($lokal["Status"] === "K") {
        echo '<div class="alert alert-success" role="alert"><strong>Det finns valsedlar här <i class="fas fa-check"></i></strong></div>';
        $done = $booked = true;
    } else {
        // Om lokalen inte är klar, är den bokad?
        if ($bookings->num_rows > 0) {
            $booking_res = $bookings->fetch_assoc();
            if ($booking_res['UserID'] === $GLOBAL_USERID){
              echo '<div class="alert alert-warning" role="alert"><strong>Denna lokal är bokad av dig! <i class="fas fa-user-clock"></i></strong></div>';
              $booked_by_me = true;
            } else {
              echo '<div class="alert alert-warning" role="alert"><strong>Denna lokal är bokad. <i class="fas fa-user-clock"></i></strong></div>';
            }
            $booked = true;
        } else {
            echo '<div class="alert alert-danger" role="alert"><strong>Det finns ingen som lägger ut här, boka gärna lokalen! <i class="fas fa-exclamation-triangle"></i></strong></div>';
        }
    }
    ?>

  </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">

    <?php
    // Är lokalen öppen för förtidsröstning?
    if ($lokal["Typ"] == "F") {
        echo '<h2>' . $lokal["lokal"] . ' <span class="badge badge-primary">Förtidsröstningslokal</span></h2>';
    } else {
        echo '<h2>' . $lokal["lokal"] . ' <small class="badge badge-info">' . 'Valdagslokal' . '</small></h2>';
    }
    ?>

  </div>
</div>

<div class="row">
  <div class="col-md-4 offset-md-2">

    <?php
    echo '<address><b>Adress</b>: ' . $lokal["Adress2"] . ($lokal["Adress1"] == '' ? '' : ' | ' . $lokal["Adress1"]) . '<br><b>Postort</b>: ' . $lokal["Postort"] . '</address>';
    echo '<p><b>Antal röstande</b>: ' . $lokal["VoterCount"] . '</p>';
    ?>

  </div>
  <div class="col-md-4" style="margin-top: 20px">

    <?php
    if (!$booked) {
        echo '<a class="btn btn-primary" href="boka_lokal.php?lokalkod=' . $lokalkod .'&action=boka_lokal" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Boka den här vallokalen <i class="fas fa-clipboard-list"></i></a>';
    } else {
        if($GLOBAL_USERID > 0) {
            $result = $conn->query('SELECT * FROM Booking JOIN vallokal ON Booking.lokalid = vallokal.lokalkod WHERE Booking.userid=' . $conn->real_escape_string($GLOBAL_USERID) . ' AND Booking.LokalID="' . $conn->real_escape_string($lokalkod) . '"');
            if ($result->num_rows > 0) {
                if (!$done) {
                  if (check_if_lokal_has_received_sedlar($conn, $lokalkod) === false){
                    echo '<a class="btn btn-warning" href="lokal_mottaget.php?lokal=' . $lokalkod . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Markera valsedlar för lokalen som mottagna <i class="fas fa-check"></i></a>';
                  }
                  echo '<a class="btn btn-success" href="lokal_klar.php?lokal=' . $lokalkod . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Markera lokalen som klar <i class="fas fa-check"></i></a>';
                  echo '<a class="btn btn-danger" href="lokal_avboka.php?lokal=' . $lokalkod . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Avboka denna lokal <i class="fas fa-times"></i></a>';
                } else {
                  echo '<a class="btn btn-info" href="lokal_oklar.php?lokal=' . $lokalkod . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Markera som ej klar <i class="fas fa-minus"></i></a>';
                }
            }
        }
    }
    echo '<a target="_blank" class="btn btn-primary" href="https://www.google.com/maps/search/?api=1&query=' . $lokal["Lat"] . ',' . $lokal["Lng"] . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Öppna karta <i class="fas fa-map"></i></a>';
    ?>

  </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">

    <?php

    if ( $booked_by_me ) {
        echo '<h3>Valsedlar</h3>';
        echo '<p>Om du behöver få valsedlar att dela ut, läs mer <a href="mina_lokaler.php#valsedlar">HÄR</a>.</p>';
    }
    echo '<table class="table table-bordered table-hover"><thead><tr><th>Riksdag</th><th>Landsting</th><th>Kommun</th></tr></thead>';
    echo '<tbody><tr><td class="table-warning">' . $lokal["AntalR"] . '</td><td class="table-primary">' . $lokal["AntL"] . '</td><td>' . $lokal["AntalK"] . '</td></tr></tbody></table>';

    echo '<h3>Öppettider</h3>';
    $array = explode(';', $lokal["Tider"]);

    echo '<table class="table-sm table-bordered table-hover">';
    foreach ($array as $arr) {
        echo '<tr><td><b>' . $arr . '</b></td></tr>';
    }
    echo '</table>';
    ?>

  </div>
</div>

<?php require("includes/footer.php");
