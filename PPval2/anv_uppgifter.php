<?php
require("includes/open_database.php");
require("includes/header.php");

if ($GLOBAL_USERID <= 0){ // Restricted to login only
  header('Location: index.php');
}


$first_name = $last_name = $adress = $post_adress = $phone = $email = '';

$result = $conn->query('SELECT * FROM User where userid=' . $conn->real_escape_string($GLOBAL_USERID));
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $first_name = $row["namn"];
        $last_name = $row["efternamn"];
        $street_adress = $row["adress"];
        $post_address = $row["postadress"];
        $phone_number = $row["telefon"];
        $email_address = $row["mail"];
    }
}
?>
<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item">Mina uppgifter</li>
      </ol>
    </nav>
  </div>
</div>

<main class="row">
  <div class="col-md-8 offset-md-2">
    <h2>Mina uppgifter</h2>
    <hr>
    <form action="anv_uppgifter_save.php" method="POST">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="first-name">Förnamn</label>
          <input type="text" class="form-control" id="first-name" name="first_name" placeholder="" value="<?php echo htmlspecialchars($first_name); ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="last-name">Efternamn</label>
          <input type="text" class="form-control" id="last-name" name="last_name" placeholder="" value="<?php echo htmlspecialchars($last_name); ?>">
        </div>
      </div>
      <div class="form-group">
        <label for="address">Adress</label>
        <input type="text" class="form-control" id="street-address" name="street_address" placeholder="" value="<?php echo htmlspecialchars($street_adress); ?>">
      </div>
      <div class="form-group">
        <label for="post-address">Postnummer och Postort</label>
        <input type="text" class="form-control" id="post-address" name="post_address" placeholder="" value="<?php echo htmlspecialchars($post_address); ?>">
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="email-address">E-postadress</label>
          <input type="email" class="form-control" id="email-address" name="email_address" placeholder="" value="<?php echo htmlspecialchars($email_address); ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="phone-number">Mobilnummer</label>
          <input type="tel" class="form-control" id="phone-number" name="phone_number" placeholder="" value="<?php echo htmlspecialchars($phone_number); ?>">
        </div>
      </div>

      <hr>

      <button type="submit" class="btn btn-primary"><i class="fas fa-caret-right"></i> Spara uppgifter <i class="fas fa-check"></i></button>
    </form>
    <a href="change_password.php" ><button class="btn btn-primary mt-3 pl-6"><i class="fas fa-caret-right"></i> Byt lösenord <i class="fas fa-key"></i></button></a>
  </div>
</main>

<script>

	$('button').on('click', function(ev) {
		if ( $('#last-name').val().length < 3 ) {
			alert('Fyll i efternamn');
			return false;
		}

		if ( $('#first-name').val().length < 3) {
			alert('Fyll i förnamn');
			return false;
		}

		if ( $('#street-address').val().length < 6 ) {
			alert('Fyll i gatuaddress');
			return false;
		}

		if ( $('#post-address').val().length < 9 ) {
			alert('Fyll i postnummer och ort');
			return false;
		}

		if ( $('#phone-number').val().length < 10 ) {
			alert('Fyll i ditt mobilnummer');
			return false;
		}




		var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		var mail = $('#email-address').val();
		var res = re.test(mail);

		if ( ! res ) {
			alert('Fyll i din mailaddress');
			return false;
		}

	});

</script>

<?php require("includes/footer.php");
