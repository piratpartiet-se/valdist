<?php
require("includes/open_database.php");

$userid = $conn->real_escape_string($GLOBAL_USERID);

if ( $userid == "-1" ) {
	header("Location: /");
	exit;
}

require("includes/header.php");

?>

<script>
function klarmarkera(thing, lokalkod) {
console.log(lokalkod);
        if(confirm('Valsedlar utlagda?')) {
		$(thing).parent().parent().find('td').first().removeClass('table-warning').addClass('table-success').text('Klar');
		$(thing).hide();
                $.get("lokal_klar.php?lokal=" + lokalkod);
        }
}
</script>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mina ansvarsområden</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-8 offset-md-2">
    <h1>Mina ansvarsområden</h1>
    </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">
  <p>
	<a href="#F" style="display: block;">Mina förtidsröstningslokaler</a>
	<a href="#V" style="display: block;">Mina vallokaler (rösta på valdagen)</a>
	<a id="menuitem-valsedlar" href="#valsedlar" style="display: none;">Ta emot valsedlar</a>
	<a id="menuitem-kommunansvarig" href="#kommunansvarig" style="display: none;">Kommunansvarig</a>
	<a id="menuitem-valdagshjalte" href="#valdagshjalte" style="display: none;">Valdagshjälte</a>
</p></div></div>
<?php


$result = $conn->query("SELECT * FROM kommunansvarig where userid = '$userid'");
 if ($result->num_rows > 0) {
	echo '<hr>';
	?>
	<script>$("#menuitem-kommunansvarig").css("display", "block");</script>
	<div class="row">
	  <div class="col-md-8 offset-md-2">
		<h2 id="kommunansvarig">Mina kommuner</h2>
	    <p>Du är kommunansvarig i följande kommuner. Gå in och kolla ofta ifall du ser att det tillkommit någon ny aktivist.</p>
	<?php
	while($row = $result->fetch_assoc()) {
		$r2 = $conn->query("SELECT * FROM Kommun where ID = $row[kommunid]");
		$kommunrow = $r2->fetch_assoc();
		echo "<h3>$kommunrow[Namn]</h3>";

		$sql = "SELECT count(*) c FROM User WHERE userid IN (SELECT UserID FROM Booking where AntalR = 0 AND LokalID IN (select LokalKod from vallokal right join (select KommunID, LänID from Kommun where ID in (select kommunid from kommunansvarig where userid = $userid)) t1 on KommunID = KommunKod and LänID = LanKod));";
		$numresult = $conn->query($sql);
		$num = $numresult->fetch_assoc()['c'];
		echo "<ul><li><a href='lamna_valsedlar.php?kommunid=$row[kommunid]'>Lämna valsedlar till aktivster.";
		if ($num > 0 ) {
			echo " <span style='background-color: red;color: white;display: inline-block;height: 24px;width: 24px;text-align: center;border-radius: 100%;'>$num</span> nya aktivster";
		}
		echo "</a></li>";

		$sql = "select count(*) c from vallokal where KommunKod = (SELECT KommunID FROM Kommun WHERE ID = $row[kommunid]) AND LanKod = (SELECT LänID FROM Kommun WHERE ID = $row[kommunid]) and Typ = 'F' and Status IN('O', '');";
		$r2 = $conn->query($sql);
		$antalObokadeFortidslokaler = $r2->fetch_assoc()['c'];
		echo "<li><a href='fget_lokaler.php?typ=f&status=obokade&kommun=$kommunrow[KommunID]&lan=$kommunrow[LänID]'>Det finns $antalObokadeFortidslokaler obokade förtidsröstningslokaler</a>";

		$sql = "select count(*) c from vallokal where KommunKod = (SELECT KommunID FROM Kommun WHERE ID = $row[kommunid]) AND LanKod = (SELECT LänID FROM Kommun WHERE ID = $row[kommunid]) and Typ = 'V' and Status IN('O', '');";
		$r2 = $conn->query($sql);
		$antalObokadeFortidslokaler = $r2->fetch_assoc()['c'];
		echo "<li><a href='fget_lokaler.php?typ=v&status=obokade&kommun=$kommunrow[KommunID]&lan=$kommunrow[LänID]'>Det finns $antalObokadeFortidslokaler obokade vallokaler (valdagen)</a>";


		$sql = "SELECT COUNT(*) c FROM valdagshjalte WHERE kommunid = $kommunrow[ID]";
		$numresult = $conn->query($sql);
		$num = $numresult->fetch_assoc()['c'];
		echo "<li><a href='lista_valdagshjaltar.php?kommunid=$row[kommunid]'>Visa valdagshjältar i $kommunrow[Namn]";
		if ($num > 0 ) {
			echo " <span style='background-color: red;color: white;display: inline-block;height: 24px;width: 24px;text-align: center;border-radius: 100%;'>$num</span>";
		}
		echo "</a></li>";

		echo "<li><a href='lista_aktivister.php?kommunid=$row[kommunid]'>Visa alla aktivister i $kommunrow[Namn]</a></li>";



		echo "</ul>";
	}
	echo '</div></div>';

 }
?>

<?php $foundBooking = false; ?>
<?php foreach(['F' => 'förtidsröstningslokaler', 'V' => 'vallokaler (rösta på valdagen)'] as $typ => $typnamn) {
   $result = $conn->query('SELECT *, vallokal.AntalR R, vallokal.AntL L, vallokal.AntalK K, vallokal.Tider T FROM Booking JOIN vallokal ON Booking.lokalid = vallokal.lokalkod WHERE vallokal.Typ = "'.$typ.'" AND Booking.userid ="' . $userid . '"');
?>
<hr>
<div class="row">
  <div class="col-md-8 offset-md-2">
	<h2 id="<?php echo $typ;?>">Mina <?php echo $typnamn; ?></h2>
<?php
if($typ == 'F' ) {
	echo '<a href="karta_mina_lokaler.html" target="_blank">Visa karta över mina förtidsröstningslokaler</a> (Kartan visar enbart dom som inte är klarmarkerade)<br><br>';
} else {
	echo '<a href="karta_mina_lokaler_valdag.html" target="_blank">Visa karta över mina valdagslokaler</a> (Kartan visar enbart dom som inte är klarmarkerade)<br><br>';
}
if ($result->num_rows > 0) { ?>
    <p>Du har anmält dig till att dela ut valsedlar på följande <?php echo $typnamn; ?>:</p>
<?php } ?>
  </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <?php

    if ($result->num_rows > 0) {
        echo '<table class="table table-bordered table-hover">';
        echo '<thead><tr><th>Status</th><th>Namn på lokal <i class="fas fa-tag"></i></th><th colspan="3">Antal<br>valsedlar</th><th>Tider</th><th>Karta <i class="fas fa-map"></th></tr></thead><tbody>';

        // output data of each row
        while($row = $result->fetch_assoc()) {
	    	$foundBooking = true;
            if ($row["Status"] == "K") {
                echo '<tr><td class="table-success">Klar</td>';
            } else if ($row["Status"] == "B") {
                echo '<tr><td class="table-warning">Bokad</td>';
            } else {
                echo '<tr><td class="table-danger">Ej bokad</td>';
            }
            echo  '<td><a href="fget_lokal.php?lokal=' . $row["LokalKod"] . '">' . $row["lokal"] . '</a><br>';

            echo ($row["Adress2"] == '' ? '??' : $row["Adress2"]) . ($row["Postort"] == '' ? '' : ', ' . $row["Postort"]) . ($row["Adress1"] == '' ? '' : ' | ' . $row["Adress1"]) . '</td>';
	    	echo "<td class='table-warning'>$row[R]</td><td class='table-primary'>$row[L]</td><td>$row[K]</td>";
			$times = $row["T"];
			$times_array = preg_split("/<br>/i", $times);

			
			$times_tooltip = preg_replace("/<br>/i", "&#013;", $times);

			$today = date_format(new DateTime("now"), "Y-m-d");
			$open_today_array = preg_grep("/".$today."/i", $times_array);
			if (count($open_today_array) >= 1) {
				$open_today = $open_today_array[array_key_first($open_today_array)];
			} else {
                                if ( $row['Typ'] == 'F' ) {
					$open_today = $today." - inte öppet";
				} else {
					$open_today = $today." - " . $times;
					}
			}

			$tomorrow = date_format(date_add(new DateTime("now"), new DateInterval("P1D")), "Y-m-d");
			$open_tomorrow_array = preg_grep("/".$tomorrow."/i", $times_array);
			if (count($open_tomorrow_array) >= 1) {
				$open_tomorrow = $open_tomorrow_array[array_key_first($open_tomorrow_array)];
			} else {
                                if ( $row['Typ'] == 'F' ) {
					$open_tomorrow = $tomorrow." - inte öppet";
				} else {
					$open_tomorrow = ""; //$tomorrow." - " . $times;
					}
			}

			echo "<td title='$times_tooltip'>$open_today<br />$open_tomorrow</td>";
       	    if ($row["Status"] != "K") {
	         echo "<td><a class='btn btn-success' onclick='klarmarkera(this, \"$row[LokalKod]\")'  role='button' style='margin: 5px; color: white;'><i class='fas fa-caret-right'></i> Klarmarkera <i class='fas fa-check'></i></a>";
            } else {
		echo '<td>';
	    }
            echo ' <a class="btn btn-primary" href="https://www.google.com/maps/search/?api=1&query=' . $row["Lat"] . ',' . $row["Lng"] . '" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Karta <i class="fas fa-map"></i></a></td></tr>';
        }
        echo '</tbody></table>';
    } else {
	echo "<p>Du har inga $typnamn bokade. <a href='index.php'>Boka nu!</a></p>";
   }
echo '</div></div>';
}
echo '<hr>';


if ( $foundBooking ) {
	echo '<div class="row"><div class="col-md-8 offset-md-2"><div id="valsedlar">&nbsp;</div>';
	echo '<script>$("#menuitem-valsedlar").css("display", "block");</script>';
	$result = $conn->query("select Kommun.ID, Kommun.Namn, sum(AntalR) AntalR, sum(AntL) AntalL, sum(AntalK) AntalK from vallokal left join Kommun on KommunKod = KommunID and LanKod = LänID where LokalKod in (select LokalID from Booking WHERE UserID = $userid) GROUP BY KommunKod, LanKod;");
	while($row = $result->fetch_assoc()) {
		echo "<h2>Ta emot valsedlar för $row[Namn]</h2>";
		$result_jag = $conn->query("SELECT * FROM kommunansvarig where kommunid = '$row[ID]' AND userid = '$GLOBAL_USERID'");
		// jag är kommunansvarig
		if ($result_jag->num_rows > 0) {
			$jag_row = $result_jag->fetch_assoc();
			echo "Du är kommunansvarig för $row[Namn]. ";
			if ( $jag_row['kollinr'] != null ) {
				echo "Valsedlar är på väg till dig! <a target='_blank' href='https://www.dhl.com/se-en/home/tracking/tracking-freight.html?submit=1&tracking-id=$jag_row[kollinr]'>Spåra</a>";
			} else {
				echo "Valsedlar kommer snart skickas till dig inom kort. Så fort vi skickas kommer du se en trackinglänk här.";
			}
		} else {
			// någon annan är kommunansvarig
			$result_annan = $conn->query("SELECT * FROM kommunansvarig where kommunid = '$row[ID]';");
			if ($result_annan->num_rows > 0) {
				//echo '<p>För att få tag på valsedlar, kontakta kommunansvarig i de kommuner du bokat upp dig på att lägga ut valsedlar i. Om du ÄR kommunansvarig så kommer du få ett paket valsedlar skickat till dig.</p>';
				echo "<p>Du behöver $row[AntalK] kommunvalsedlar, $row[AntalL] landstingsvalsedlar samt $row[AntalR] riksdagsvalsedlar för dina lokaler i $row[Namn]. ";
		        	echo 'För att få tag på valsedlar, kontakta kommunansvarig;</p>';

				while($row_annan = $result_annan->fetch_assoc()) {
					$r2 = $conn->query("SELECT * FROM User where UserID = $row_annan[userid]");
					$userrow = $r2->fetch_assoc();
					echo "<p>" . htmlspecialchars($userrow['namn']) . " " . htmlspecialchars( $userrow['efternamn']) . "<br>" . htmlspecialchars($userrow['adress']) ."<br>" . htmlspecialchars($userrow['postadress']) . "<br>Telefon: " . htmlspecialchars($userrow['telefon']) . "<br>Mail: <a href='mailto:" . htmlspecialchars($userrow['mail']) . "'>". htmlspecialchars($userrow['mail']) . "</a></p>";
				}
			} else {

				$r4 = $conn->query("SELECT * FROM Kommun where ID = '$row[ID]'");
				$kommunrow2 = $r4->fetch_assoc();
				echo	"<p>Det finns ingen kommunansvarig! Kommunansvarig är den som vi skickar valsedlarna till. Kan du ställa upp med att vara mottagare? <a href='kommunansvarig.php?kommun=$kommunrow2[KommunID]&lan=$kommunrow2[LänID]'>Läs mer här.</a></p>";
			}
		}
	}
	echo '</div></div>';
	echo '<hr>';
}





$result = $conn->query("SELECT * FROM valdagshjalte where userid = '$userid'");
 if ($result->num_rows > 0) {
	?>
	<script>$("#menuitem-valdagshjalte").css("display", "block");</script>
	<div class="row">
	  <div class="col-md-8 offset-md-2">
		<h2 id="valdagshjalte">Kommuner du är valdagshjälte i</h2>
		    <p>En valdagshjälte är någon som vill göra lite extra, någon som inte bara kan tänka sig att fara till den närmaste vallokalen, utan som faktiskt kan tänka sig att göra en insats där det behövs.</p>
		    <p>Som valdagshjälte så sammarbetar du med kommunansvarig för att på valdagen (och när förtidsröstningen börjar) se vilka lokaler som inte någon annan lägger ut valsedlar i. Sedan så åker du till så många av dessa som du vill, kan eller orkar och ser till att det finns valsedlar där.</p>
		    <p>Du ser kontaktuppgifter till kommunansvarig nedan. Ta kontakt med dom så snart som möjligt för att stämma av.</p>
	<?php
	while($row = $result->fetch_assoc()) {
		$r2 = $conn->query("SELECT * FROM Kommun where ID = $row[kommunid]");
		$kommunrow = $r2->fetch_assoc();
		echo "<h3>$kommunrow[Namn]</h3>";
		print_kommunansvarig($conn, $row['kommunid']);
	}
	echo '</div></div>';
 }


 function print_kommunansvarig($conn, $kommunid) {
	$kommunid = (int)$kommunid;
	$result = $conn->query("SELECT * FROM kommunansvarig where kommunid = '$kommunid'");
	$r3 = $conn->query("SELECT * FROM Kommun where ID = '$kommunid'");
	$kommunrow = $r3->fetch_assoc();

	 echo "<h4>Kommunansvarig i $kommunrow[Namn]</h4>";
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$r2 = $conn->query("SELECT * FROM User where UserID = $row[userid]");
			$userrow = $r2->fetch_assoc();
			echo "<p>$userrow[namn] $userrow[efternamn]<br>$userrow[adress]<br>$userrow[postadress]</p><p>Telefon: $userrow[telefon]</p><p>Mail: <a href='mailto:$userrow[mail]'>$userrow[mail]</a></p>";
		}
	} else {
		echo	"<p>Det finns ingen kommunansvarig! Kommunansvarig är den som vi skickar valsedlarna till. Kan du ställa upp med att vara mottagare? <a href='kommunansvarig.php?kommun=$kommunrow[KommunID]&lan=$kommunrow[LänID]'>Läs mer här.</a></p>";
	}
}
require("includes/footer.php");
