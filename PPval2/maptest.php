<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Testa Kartjävlar Här</title>
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<link href="https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.js"></script>
<script src="render_map.js"></script>
<style>
body { margin: 0; padding: 0; }
#map { position: absolute; top: 0; bottom: 0; width: 100%; }
</style>
</head>
<body>
  <div id="map_div" style="height:500px"></div>
  <script>
  mapLoader = new CompleteMap('map_div', 'pk.eyJ1Ijoiam9oYW4ta2FybHNzb24tcGlyYXRwYXJ0aWV0LXNlIiwiYSI6ImNsNzI5amIwcDAybmYzbm53YjAxbDM2em0ifQ.Ha3FOkld0e9zZ75l6JuTBw', 'OppnaIdag', 'api_lokaldata.php')
  </script>
<body>
