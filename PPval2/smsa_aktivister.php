<?php
require("includes/open_database.php");

set_time_limit(7200);
ini_set('max_execution_time',7200);


if (!$isadmin) {
	header("Location: /");
	exit;
}

require("includes/header.php");

echo '<main class="row">  <div class="col-md-8 offset-md-2">';


if(isset($_GET['done']) && $_GET['done'] == 1) {
	echo '<h1>Klart!</h1>';
}

//$result = $conn->query("SELECT concat(namn, ' ', efternamn) namn, concat(adress, ' ', postadress) address, mail, telefon FROM User WHERE UserID IN (select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod  = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID GROUP BY kommunansvarig.UserID);");
//$result = $conn->query("select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID;");

//$result = $conn->query("select KommunKod, LanKod from Booking LEFT JOIN vallokal ON LokalID = LokalKod WHERE Booking.AntalR = 0 group by KommunKod, LanKod;");

//$result = $conn->query("select * from User WHERE UserID IN (select DISTINCT UserID FROM (select Booking.UserID from Booking LEFT JOIN vallokal ON LokalID = vallokal.LokalKod left join Kommun ON vallokal.KommunKod = Kommun.KommunID and vallokal.LanKod = Kommun.LänID left join kommunansvarig on Kommun.ID = kommunansvarig.kommunid where kommunansvarig.userid IS NULL) t1);");
$result = $conn->query("select * from User WHERE UserID IN (select DISTINCT UserID FROM (select Booking.UserID from Booking LEFT JOIN vallokal ON LokalID = vallokal.LokalKod WHERE vallokal.Typ = 'V') t1);");
$numbers = [];
echo '<table class="table table-bordered table-hover">';

while($row = $result->fetch_assoc()) {
//	$result2 = $conn->query("SELECT * FROM Booking WHERE Booking.LokalID IN (select LokalKod from Kommun RIGHT JOIN vallokal ON KommunID = KommunKod AND LänID = LanKod WHERE Kommun.ID = $row[kommunid]) and Booking.UserID != $row[userid] AND Booking.AntalR = 0;");
//	while($row2 = $result2->fetch_assoc()) {
//		$result3 = $conn->query("SELECT * FROM User WHERE UserID = $row[userid]");
//		while($row3 = $result3->fetch_assoc()) {
			$numbers[$row['telefon']] = $row['telefon'];
			echo '<tr><td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td><td>' . htmlspecialchars($row['adress'] . ' '  . $row['postadress']) . '</td><td>' . htmlspecialchars($row['mail'])  . '</td><td>' . htmlspecialchars($row['telefon']) . '</td></tr>';
//		}
//	}
}

echo '</table>';

echo 'Totalt kommer ' . count($numbers) . ' SMS gå ut. ';
echo '<a href="smsa_aktivister.php?send=1">Skicka</a>';

echo '</div></main>';

if(isset($_GET['send']) && $_GET['send'] == 1) {
	foreach($numbers as $i => $number) {
//		file_put_contents($log, "$i: $number\n", FILE_APPEND);
		echo "$i: $number<br>";
		$sms = array(
		  "from" => "PP",   /* Can be up to 11 alphanumeric characters */
		  "to" => $number,  /* The mobile number you want to send to */
//		  "message" => "Piratpartiet: Hej! I morgon är det valdagen och det är dags att lämna valsedlar till vallokaler! Beroende på var du bor så går det att lämna valsedlar från klockan 07 eller 07:30 på morgonen. När du lämnat valsedlarna, se till att klicka på 'Klar'-knappen för lokalen under 'Mina ansvarsområden'. PS. För dig som har många vallokaler att gå till så kan översiktskartan över dina lokaler vara användbar: https://valsedel.piratpartiet.se/karta_mina_lokaler_valdag.html (Men du kanske måste logga in först på https://valsedel.piratpartiet.se/ för att länken ska fungera.) Frågor? SMSa Johan på 0737689268 eller maila johan.karlsson@piratpartiet.se Lycka till i morgon!"
		"message" => "Piratpartiet: Hej! Hoppas allting går bra med valsedelsutläggandet! Vi har haft en bugg i systemet nu på morgonen så att klarmarkeringarna har inte alltid fungerat. Därför vill vi be alla ladda om sidan ( https://valsedel.piratpartiet.se/mina_lokaler.php ) och bekräfta att det fungerade. Om det inte gjorde det, prova igen, nu ska det fungera. Något problem med något? Hör av dig till Johan på 0737689268"
		);
		sleep(1);
		$res = sendSMS($sms);
		echo $res;
		flush();
	}
//	header("Location: smsa_kommunansvariga.php?done=1");

}

exit;
