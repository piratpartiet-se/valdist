<?php
require("includes/open_database.php");

$email = isset($_POST["email"]) ? $_POST["email"] : 'NEJ';
$password = isset($_POST['password-login']) ? $_POST['password-login'] : '';

$login_result = login($conn, $email, $password);
if ($login_result === true){
    header('Location: /');
} else {
    echo '<p style="font-size:14px;background:red;">' . $login_result . '</p>';
}
exit();
