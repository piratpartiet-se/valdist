<?php
require("includes/open_database.php");
require("includes/header.php");

$userid = $conn->real_escape_string($GLOBAL_USERID);
$kommunid = $conn->real_escape_string($_GET['kommunid']);

?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item"><a href="mina_lokaler.php">Mina ansvarsområden</a></li>
	<li class="breadcrumb-item active" aria-current="page">Alla aktivister</li>
      </ol>
    </nav>
  </div>
</div>
<?php

 $result = $conn->query("SELECT * FROM kommunansvarig where userid = '$userid' and kommunid = '$kommunid'");
 if ($result->num_rows > 0) {
	$r2 = $conn->query("SELECT * FROM Kommun where ID = '$kommunid'");
	$kommunrow = $r2->fetch_assoc();
	?>
	<script>$("#menuitem-kommunansvarig").css("display", "block");</script>
	<div class="row">
	  <div class="col-md-8 offset-md-2">
		<h2 id="valdagshjalte">Alla aktivister i <?php echo $kommunrow['Namn']; ?></h2>

	<?php
	while($row = $result->fetch_assoc()) {
		$typ = '';
		if ( isset($_GET['typ'] )) {
			$typ = $_GET['typ'] == 'F' ? " Typ = 'F' AND " : " Typ = 'V' AND ";
		}
		$sql = "select * from User where UserID in (select UserID from Booking where LokalID in (select lokalkod from vallokal where $typ kommunkod = $kommunrow[KommunID] and lankod = $kommunrow[LänID]));";
		$aktivistresult = $conn->query($sql);
		$print_stupid_ass_backwards_stuff = true;
		while($userrow = $aktivistresult->fetch_assoc()) {
			$print_stupid_ass_backwards_stuff = false;

			echo "<p>".htmlspecialchars($userrow['namn'])." ".htmlspecialchars($userrow['efternamn'])."<br>".htmlspecialchars($userrow['adress'])."<br>".htmlspecialchars($userrow['postadress'])."</p>";
			echo "<p>Telefon: ".htmlspecialchars($userrow['telefon'])."</p><p>Mail: <a href='mailto:" .htmlspecialchars($userrow['mail'])."'>".htmlspecialchars($userrow['mail'])."</a></p>";
			

			$lokaler = $conn->query("SELECT concat(lokal, ' ', Adress1) lokal FROM vallokal where Typ = 'F' AND lokalkod in (select LokalID from Booking WHERE UserID = $userrow[UserID]);");
			if ($lokaler->num_rows > 0 ) {
				echo '<strong>Förtidsröstningslokaler:</strong><br>';
			}
			while($lokalrow = $lokaler->fetch_assoc()) {
				echo htmlspecialchars($lokalrow['lokal']) . '<br>';
			}

			$lokaler = $conn->query("SELECT concat(lokal, ' ', Adress1) lokal FROM vallokal where Typ = 'V' AND lokalkod in (select LokalID from Booking WHERE UserID = $userrow[UserID]);");
			if ($lokaler->num_rows > 0 ) {
				echo '<strong>Vallokaler:</strong><br>';
			}
			while($lokalrow = $lokaler->fetch_assoc()) {
				echo htmlspecialchars($lokalrow['lokal']) . '<br>';
			}
			echo '<hr>';
		}
	}
	if ($print_stupid_ass_backwards_stuff === true){
		echo "<p>Det finns för närvarande inga aktivister i $kommunrow[Namn] :(</p>";
	}
	echo '</div></div>';
 }
require("includes/footer.php");
