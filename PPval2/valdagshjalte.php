<?php
require("includes/open_database.php");
require("includes/header.php");
?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <?php


        $lannr = $_GET["lan"];
        $result = $conn->query("SELECT * FROM Län WHERE LänID=" . $conn->real_escape_string($lannr));
        echo '<li class="breadcrumb-item active" aria-current="page">' . $result->fetch_assoc()["Namn"] . '</li>';
        ?>
      </ol>
    </nav>
  </div>
</div>

<main class="row">
  <div class="col-md-8 offset-md-2">
    <p>En valdagshjälte är någon som vill göra lite extra, någon som inte bara kan tänka sig att fara till den närmaste vallokalen, utan som faktiskt kan tänka sig att göra en insats där det behövs.</p>
    <p>Som valdagshjälte så sammarbetar du med kommunansvarig för att på valdagen (och när förtidsröstningen börjar) se vilka lokaler som inte någon annan lägger ut valsedlar i. Sedan så åker du till så många av dessa som du vill, kan eller orkar och ser till att det finns valsedlar där.</p>
    <p>Låter det här som något för dig?</p>
    <p><a href="boka_valdagshjalte.php?kommun=<?php echo $_GET['kommun'];?>&lan=<?php echo $_GET['lan'];?>">Ja, absolut, sign me up!</a></p>
    <p><a href="fget_lokaler.php?typ=v&kommun=<?php echo $_GET['kommun'];?>&lan=<?php echo $_GET['lan'];?>">Nja, jag vill välja lokaler själv</a></p>
  </div>
</main>


<?php require("includes/footer.php"); ?>
