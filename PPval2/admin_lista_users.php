<?php
require("includes/open_database.php");

set_time_limit(7200);
ini_set('max_execution_time',7200);


if (!$isadmin) {
	header("Location: /");
	exit;
}

if(isset($_GET['userid'])) {
	$toset = (int)$_GET['userid'];
	$conn->query("update User set Admin = 1 WHERE UserID = $toset;");
}

require("includes/header.php");

echo '<main class="row">  <div class="col-md-8 offset-md-2">';


$result = $conn->query("select * from User;");
echo '<table class="table table-bordered table-hover">';

while($row = $result->fetch_assoc()) {
	echo '<tr><td>'.$row['UserID'].'</td><td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td><td>' . htmlspecialchars($row['adress'] . ' '  . $row['postadress']) . '</td><td>' . htmlspecialchars($row['mail'])  . '</td><td>' . htmlspecialchars($row['telefon']) . '</td><td><a href="admin_lista_users.php?userid='.$row['UserID'].'">'. ($row['Admin'] ? 1 : 0) .'</td></tr>';
}

echo '</table>';

echo '</div></main>';
