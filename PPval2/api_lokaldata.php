<?php
if(!function_exists('utf8_encode_if_needed')) {
    function utf8_encode_if_needed($text){
        $test = @json_encode($text);
        if($test == "null" || $test === false) {
            $res = utf8_encode($text);
            return $res;
        }
        return $text;
    }
}
require("includes/open_database.php");
$userid = $conn->real_escape_string($GLOBAL_USERID);
$action = isset($_GET['action']) ? $_GET['action'] : null;

switch ($action) {
    case 'Fortidsrostningslokaler':
      $dataset = get_fortidsrostningslokaler($conn);
      break;
    case 'Valdagslokaler':
      $dataset = get_valdagslokaler($conn);
      break;
    case 'MinaBokade':
      $dataset = get_lokaler_by_userid($conn, $userid);
      break;
    case 'MinaAvklarade':
      $dataset = get_avklarade_lokaler_by_user($conn, $userid);
      break;
    case 'MinaEjAvklarade':
      $dataset = get_ej_avklarade_lokaler_by_user($conn, $userid);
      break;
    case 'OppnaIdag':
      $dataset = get_lokaler_oppna_idag($conn);
      break;
    case 'OppnaIdagOchImorgon':
      $dataset = get_lokaler_oppna_idag_och_imorgon($conn);
      break;
    default:
      $dataset = get_all_lokaler($conn);
}

header('Content-Type: application/json; charset=utf-8');
echo utf8_encode_if_needed(json_encode($dataset));
