<?php
require("includes/open_database.php");


//Form validation - måste göras innan någon output eftersom PHP7 inte grejar att sätta headers efter output
if (isset($_POST['new_password']) === true && isset($_POST['new_password_confirmation'])){
  if (strlen($_POST['new_password']) < 8){
    $err = 'Lösenordet måste vara minst 8 tecken';
  } else if ($_POST['new_password'] !== $_POST['new_password_confirmation']) {
    $err = 'Lösenorden stämmer inte';
  } else if (strlen($_POST['new_password']) >= 8 && $_POST['new_password'] === $_POST['new_password_confirmation']){
    set_password_for_user($conn, $GLOBAL_USERID, $_POST['new_password']);
    header('Location: anv_uppgifter.php');
  }
}

require('includes/header.php');
?>
<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item"><a href="anv_uppgifter.php">Mina uppgifter</a></li>
        <li class="breadcrumb-item">Byt lösenord</li>
      </ol>
    </nav>
  </div>
</div>
<main class="row">

          <div class="col-md-8 offset-md-2">
            <h2>Byt lösenord</h2>
            <p>Här väljer du ett nytt lösenord (minst åtta tecken).</p>
            <hr>
          </div>
          <div class="col-md-8 offset-md-2">
            <?php

              if (isset($err)){
                // Rita ett felmeddelande
                echo '<div class="alert alert-danger" role="alert">Fel: '.$err.'</div>';
                echo '</div><div class="col-md-8 offset-md-2">';
              }
            ?>

            <form method="post">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="password">Lösenord</label>
                  <input type="password" class="form-control" id="new-password" name="new_password" placeholder="" value="">
                </div>
                <div class="form-group col-md-6">
                  <label for="password">Bekräfta Lösenord</label>
                  <input type="password" class="form-control" id="new-password-confirmation" name="new_password_confirmation" placeholder="" value="">
                </div>
              </div>
              <hr>
              <button id="pwdChangeBtn" type="submit" class="btn btn-primary"><i class="fas fa-caret-right"></i> Byt lösenord <i class="fas fa-clipboard-list"></i></button>
            </form>
          </div>
        </main>

<script>
$('#pwdChangeBtn').on('click', function(ev) {
  if ( $('#new-password').val().length < 8 ) {
    alert('Lösenordet måste vara minst 8 tecken');
    return false;
  }

  if ($('#new-password').val() !== $('#new-password-confirmation').val()) {
    alert('Lösenorden stämmer inte');
    return false;
  }
}
</script>
