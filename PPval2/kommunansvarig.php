<?php
require("includes/open_database.php");
require("includes/header.php");
?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <?php


        $lannr = $_GET["lan"];
        $result = $conn->query("SELECT * FROM Län WHERE LänID=" . $conn->real_escape_string($lannr));
        echo '<li class="breadcrumb-item active" aria-current="page">' . $result->fetch_assoc()["Namn"] . '</li>';
        ?>
      </ol>
    </nav>
  </div>
</div>

<main class="row">
  <div class="col-md-8 offset-md-2">
    <p>Titeln kommunansvarig låter kanske mer läskigt än den egentligen är. Att vara kommunansvarig innebär att du får en låda med valsedlar skickad till dig.</p>
    <p>Ditt jobb är sedan att ta kontakt med de andra aktivisterna i kommunen för att fördela valsedlarna mellan er. Här på sidan finns en lista med namn, telefonnummer och mail till alla som skrivit upp sig för att hjälpa till att lägga ut valsedlar i en eller flera vallokaler i din kommun.</p>
    <p>Om det är så att du bor i en mindre kommun så kan det mycket väl hända att du är enda aktivisten i kommunen. I så fall behöver du bara prata med dig själv ;) (Även om det ju är uppskattat ifall du ber släkt och vänner om hjälp så är det inget som förväntas av dig)</p>
    <p>Och kom ihåg - även om det är så att det bara kommer ut valsedlar i en eller två vallokaler så är det ändå mycket bättre än att det inte kommer ut i någon :)</p>

    <p>Låter det här som något för dig?</p>
    <p><a href="boka_kommunansvarig.php?kommun=<?php echo $_GET['kommun'];?>&lan=<?php echo $_GET['lan'];?>">Ja, absolut, sign me up!</a></p>
    <p><a href="fget_lokaler.php?typ=v&kommun=<?php echo $_GET['kommun'];?>&lan=<?php echo $_GET['lan'];?>">Nja, det låter för läskigt, gå tillbaka</a></p>
  </div>
</main>


<?php require("includes/footer.php"); ?>
