<?php
require("includes/open_database.php");

$userid = $conn->real_escape_string($GLOBAL_USERID);

if ($_GET['what'] == 'mina_ohanterade') {
	$typ = 'F';
	if ( isset($_GET['typ']) && $_GET['typ'] == 'V' ) {
		$typ = 'V';
	}

	$sql = 'SELECT vallokal.lokal, vallokal.LokalKod, vallokal.Lat, vallokal.Lng, vallokal.lokal, Adress1, Adress2, vallokal.AntalR R, vallokal.AntL L, vallokal.AntalK K, vallokal.Tider T FROM Booking JOIN vallokal ON Booking.lokalid = vallokal.lokalkod WHERE vallokal.Typ = "'.$typ.'" AND Status != "K" AND Booking.userid ="' . $userid . '"';

	$result = $conn->query($sql);

	$stuff = [];
	while($row = $result->fetch_assoc()) {
		$times = $row["T"];
		$times_array = preg_split("/<br>/i", $times);

		
		$times_tooltip = preg_replace("/<br>/i", "&#013;", $times);

		$today = date_format(new DateTime("now"), "Y-m-d");
		$open_today_array = preg_grep("/".$today."/i", $times_array);
		if (count($open_today_array) >= 1) {
			$open_today = $open_today_array[array_key_first($open_today_array)];
		} else {
			if ( $row['Typ'] == 'F' ) {
				$open_today = $today." - inte �ppet";
			} else {
				$open_today = $today . " - " . $times;
			}
		}

		$tomorrow = date_format(date_add(new DateTime("now"), new DateInterval("P1D")), "Y-m-d");
		$open_tomorrow_array = preg_grep("/".$tomorrow."/i", $times_array);
		if (count($open_tomorrow_array) >= 1) {
			$open_tomorrow = $open_tomorrow_array[array_key_first($open_tomorrow_array)];
		} else {
//			$open_tomorrow = $tomorrow." - inte �ppet";
			if ( $row['Typ'] == 'F' ) {
//				$open_tomorrow = '-';
				$open_tomorrow = $tomorrow." - inte �ppet";
			} else {
				$open_tomorrow = '-';
			}

		}
		$row['today'] = base64_encode($open_today);
		$row['tomorrow'] = base64_encode($open_tomorrow);
		
		$stuff[] = $row;

	}
} else if ($_GET['what'] == 'obokade' || $_GET['what'] == 'obokade_valdag' || $_GET['what'] == 'alla_valdag') {
	$typ = $_GET['what'] == 'obokade' ? 'F' : 'V';

	$sql = 'SELECT vallokal.lokal, vallokal.LokalKod, vallokal.Lat, vallokal.Lng, vallokal.lokal, Adress1, Adress2, vallokal.AntalR R, vallokal.AntL L, vallokal.AntalK K, vallokal.Tider T FROM vallokal WHERE vallokal.Typ = "'.$typ.'" AND (Status = "o" OR Status = "O" OR Status = "");';
	if($_GET['what'] == 'alla_valdag') {
		$sql = 'SELECT vallokal.lokal, vallokal.LokalKod, vallokal.Lat, vallokal.Lng, vallokal.lokal, Adress1, Adress2, vallokal.AntalR R, vallokal.AntL L, vallokal.AntalK K, vallokal.Tider T, if((Status = "o" OR Status = "O" OR Status = ""), "blue", "red") as color  FROM vallokal WHERE vallokal.Typ = "'.$typ.'"';
	}

	$result = $conn->query($sql);

	$stuff = [];
	while($row = $result->fetch_assoc()) {
		$times = $row["T"];
		$times_array = preg_split("/<br>/i", $times);

		
		$times_tooltip = preg_replace("/<br>/i", "&#013;", $times);

		$today = date_format(new DateTime("now"), "Y-m-d");
		$open_today_array = preg_grep("/".$today."/i", $times_array);
		if (count($open_today_array) >= 1) {
			$open_today = $open_today_array[array_key_first($open_today_array)];
		} else {
			if ( $row['Typ'] == 'F' ) {
//				$open_today = $today . " - " . $times;
				$open_today = $today." - inte �ppet";
			} else {
				$open_today = $today . " - " . $times;
			}
//			$open_today = $today." - inte �ppet";
		}

		$tomorrow = date_format(date_add(new DateTime("now"), new DateInterval("P1D")), "Y-m-d");
		$open_tomorrow_array = preg_grep("/".$tomorrow."/i", $times_array);
		if (count($open_tomorrow_array) >= 1) {
			$open_tomorrow = $open_tomorrow_array[array_key_first($open_tomorrow_array)];
		} else {
			if ( $row['Typ'] == 'F' ) {
//				$open_tomorrow = '-';
				$open_tomorrow = $tomorrow." - inte �ppet";
			} else {
				$open_tomorrow = '-';
			}
		}
		$row['today'] = base64_encode($open_today);
		$row['tomorrow'] = base64_encode($open_tomorrow);
		
		$stuff[] = $row;

	}
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($stuff);
