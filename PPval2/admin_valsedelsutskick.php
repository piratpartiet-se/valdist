<?php
require("includes/open_database.php");

if (!$isadmin) {
        header("Location: /");
	exit;
}

require("includes/header.php");

$sql = "select User.UserID, namn, efternamn, adress, postadress, telefon, mail from kommunansvarig left join User on kommunansvarig.userid = User.UserID and coalesce(kollinr, '') = '' group by User.UserID having namn != '' order by User.UserID;";


echo '<div class="row">
  <div class="col-md-8 offset-md-2">';

echo '<h1>Frakter som behöver bokas</h1>';
echo '<form action="save_valsedelsutskick.php" method="post">';
echo '<table class="table table-bordered table-hover">';
echo '<thead><tr>
		<th>Namn</th>
		<th>Address</th>
		<th>Postnr</th>
		<th>Postort</th>
		<th>Telefon</th>
		<th>Mail</th>
		<th>Vikt</th>
		<th>Boka</th>
		<th>Tracking #</th>
	</tr></thead>';

$result = $conn->query($sql);

while($row = $result->fetch_assoc()) {
        echo '<tr>';
        echo '<td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td>';
        echo '<td>' . htmlspecialchars($row['adress']) . '</td>';
        $postnr = '';
	$postort = '';
        foreach(str_split($row['postadress']) as $char) {
		if (is_numeric($char)) {
			$postnr .= $char;
		} else {
			$postort .= $char;
		}
	}
	$postort = trim($postort);
	$postnr = trim($postnr);
	$sql = "select round((sum(AntalR+AntL+AntalK)*1.28+250)/1000*1.1, 1) vikt from (select * from Kommun where ID in (select kommunid from kommunansvarig where coalesce(kollinr, '') = '' and UserID = $row[UserID])) t1 RIGHT JOIN vallokal on t1.KommunID = vallokal.KommunKod and t1.LänID = vallokal.LanKod where ID is not null;";
	$result2 = $conn->query($sql);
	if($result2 !== false) {
	        $row2 = $result2->fetch_assoc();
	} else {
		$row2 = ['vikt' => '-'];
	}

        echo '<td>' . htmlspecialchars($postnr) . '</td>';
        echo '<td>' . htmlspecialchars($postort) . '</td>';
        echo '<td>' . htmlspecialchars($row['telefon']) . '</td>';
        echo '<td>' . htmlspecialchars($row['mail']) . '</td>';
        echo '<td>'.$row2['vikt'].' kg</td>';
        echo '<td><a href="https://piratprylar.se/valsedel/fraktbokning.php?packlista=Valsedlar&vikt='.$row2['vikt'].'&namn='.urlencode($row['namn'] . ' ' . $row['efternamn']).'&gata='.urlencode($row['adress']).'&postnr='.$postnr.'&mobil='.preg_replace('/\s+/', '', $row['telefon']).'&epost='.trim($row['mail']).'&submit=Spara+%C3%A4ndringar">Boka</a></td>';
        echo '<td><input type="text" name="userid-'.$row['UserID'].'"</td>';
	echo '</tr>';

}
echo '</table>';
echo '<button type="submit">Skicka</button>';
echo '</div></div>';

require("includes/footer.php");
