<?php require("mobile_detect.php"); ?>
<!DOCTYPE html>
<html lang="sv">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Piratpartiet: valsedelsdistribution</title>

  <meta name="description" content="PP valsedelssystem">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,Piratpartiet,valsedlar,system">
  <meta name="author" content="Johan Roos Tibbelin">
  <link rel="icon" type="image/png" href="favicon.png">

<script src="assets/scripts/jquery-3.6.0.js"></script>
<script src="assets/scripts/jquery-ui.min.js"></script>


  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/solid.css" integrity="sha384-VGP9aw4WtGH/uPAOseYxZ+Vz/vaTb1ehm1bwx92Fm8dTrE+3boLfF1SpAtB1z7HW" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/fontawesome.css" integrity="sha384-1rquJLNOM3ijoueaaeS5m+McXPJCGdr5HcA03/VHXxcp2kX2sUrQDmFc3jR5i/C7" crossorigin="anonymous">

  <link rel="stylesheet" href="assets/css/jquery-ui.css">
</head>
<body>
<div id="wrapper" class="container-fluid">
  <header class="row">
    <div class="col-md-8 offset-md-2">
      <a href="index.php"><img src="assets/icons/pirat-logga.png" alt="Piratpartiets logga" <?php if ($mobile_browser) { echo ' width="300"';} ?>></a>
    </div>
  </header>
  <hr>
  <div class="row" <?php if ( basename($_SERVER["SCRIPT_FILENAME"], '.php') == 'forgot_password' ) { echo 'style="display: none;"'; } ?> >
  <div class="col-md-8 offset-md-2">
    <?php
    if($GLOBAL_USERID > 0) {
        //Mina Ansvarsområden
        if (does_user_have_responsibilities($conn, $GLOBAL_USERID)){
          echo '<a class="btn btn-pirate" href="mina_lokaler.php" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Mina ansvarsområden <i class="fas fa-city"></i></a>';
        }
        echo '<a class="btn btn-pirate" href="anv_uppgifter.php" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Profil <i class="fas fa-user"></i></a>' . '<a class="btn btn-pirate" href="logout.php" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Logga ut <i class="fas fa-user-slash"></i></a>';
    } else {
        echo '<form class="form-inline" action="login.php" method="POST"><label class="sr-only" for="email">E-postadress</label><div class="input-group mb-2 mr-sm-2"><div class="input-group-prepend"><div class="input-group-text">@</div></div><input type="email" class="form-control" id="email" name="email" placeholder="E-postadress"></div><div class="input-group mb-2 mr-sm-2"><div class="input-group-prepend"><div class="input-group-text">*</div></div><input type="password" class="form-control" id="password-login" name="password-login" placeholder="Lösenord"></div><button type="submit" class="btn btn-primary mb-2"><i class="fas fa-caret-right"></i> Logga in</button><a href="registrera.php"><div class="btn btn-primary mb-2 ml-5"><i class="fas fa-caret-right"></i> Registrera</div></a>';
        echo '<a href="forgot_password.php"><div class="btn btn-primary mb-2 ml-5"><i class="fas fa-caret-right"></i> Glömt lösenord?</div></a>'; // Slå på när vi löst mail-helvetet
        echo '</form>';

    }
    if (isset($isadmin) &&  $isadmin ) {
        echo '<a class="btn btn-pirate" href="admin.php" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Admin <i class="fas fa-space-shuttle"></i></a>';
    }
    ?>
    <a class="btn btn-pirate" href="faq.php" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> FAQ <i class="fas fa-question-circle"></i></a>
    <hr>
  </div>
</div>
<div class="row">
  <div class="col-md-8 offset-md-2">
    <div class="alert alert-primary" role="alert">Hittade du ett fel i systemet? Vänligen meddela <a class="alert-link" href="mailto:johan.karlsson@piratpartiet.se">johan.karlsson@piratpartiet.se</a>. Tack på förhand. <i class="fas fa-heart"></i></div>
<!--    <div class="alert alert-danger" role="alert">Valdagen börjar närma sig med stora kliv och det är hög tid att boka vallokaler om vi ska ha möjlighet att skicka valsedlar innan klockan <strong>15:00 idag den 6/9</strong>. Om du missar detta och har möjlighet att hämta upp valsedlar, vänligen kontakta valsedelansvariga.</div> -->
  </div>
</div>
