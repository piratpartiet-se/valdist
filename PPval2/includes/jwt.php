<?php
// Functions for working with JWT
$JWT_HEADERS = array('alg'=>'HS256','typ'=>'JWT');

function generate_jwt($payload, $secret=null) {
  if ($secret === null){
      $secret = $GLOBALS['app_secret'];
  }
	$headers_encoded = base64url_encode(json_encode($GLOBALS['JWT_HEADERS']));

	$payload_encoded = base64url_encode(json_encode($payload));

	$signature = hash_hmac('SHA256', "$headers_encoded.$payload_encoded", $secret, true);
	$signature_encoded = base64url_encode($signature);

	return "$headers_encoded.$payload_encoded.$signature_encoded";
}

function base64url_encode($str) {
    return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
}

function get_jwt_payload($jwt) {
	// split the jwt
	$tokenParts = explode('.', $jwt);
	$header = base64_decode($tokenParts[0]);
	$payload = base64_decode($tokenParts[1]);
	$signature_provided = $tokenParts[2];
  return json_decode($payload, true);
}

function validate_jwt_format($jwt){
  if($jwt === null){
    return false;
  }
  if (strlen((string)$jwt) === 0){
    return false;
  }
  $tokenParts = explode('.', $jwt);
  if (count($tokenParts) !== 3){
    return false;
  }
  if (base64_decode($tokenParts[0]) !== json_encode($GLOBALS['JWT_HEADERS'])){
    return false;
  }
  if (is_array(json_decode(base64_decode($tokenParts[1]), true)) === false){
    return false;
  }
  return true;
}

function validate_jwt($jwt, $secret=null) {
  if ($secret === null){
      $secret = $GLOBALS['app_secret'];
  }
  if (validate_jwt_format($jwt) === false){
    return false;
  }

	// split the jwt
	$tokenParts = explode('.', $jwt);
	$header = base64_decode($tokenParts[0]);
	$payload = base64_decode($tokenParts[1]);
	$signature_provided = $tokenParts[2];

	// build a signature based on the header and payload using the secret
	$base64_url_header = base64url_encode($header);
	$base64_url_payload = base64url_encode($payload);
	$signature = hash_hmac('SHA256', $base64_url_header . "." . $base64_url_payload, $secret, true);
	$base64_url_signature = base64url_encode($signature);

	// verify it matches the signature provided in the jwt
	return ($base64_url_signature === $signature_provided);
}
