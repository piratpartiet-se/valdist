<?php
ini_set('session.gc_maxlifetime', 2592000);

if (!function_exists('array_key_first')){
               function array_key_first(array $array) { foreach ($array as $key => $value) { return $key; } }
}


session_set_cookie_params(2592000);
session_start();
include("config.php");

// Assert that DB settings is set, or default when possible

if(isset($servername) === false){
  $servername = "localhost";
}
if(isset($port) === false){
  $port = "3306";
}
if(isset($username) === false && isset($password) === false && isset($dbname) === false){
  die("Missing configuration parameters!");
}

// Create connection
$conn = new mysqli($servername . ":" . $port, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Setup session_vars
if(isset($GLOBALS['app_secret']) === false){ // Jätte-jätte-jättefailsafe hörrö
  // Inget använder den explicit (än), men om man vill ha en site-specifik secret för krypto så...
  $GLOBALS['app_secret'] = hash('sha256', $servername.$port.$username.$password.$dbname);
}


if(isset($_SESSION['UserID'])){
  $GLOBAL_USERID = $_SESSION['UserID'];
} else {
  $GLOBAL_USERID = -1;
}

$isadmin = 0;
if ( $GLOBAL_USERID != -1) {
    $result = $conn->query("SELECT Admin from User where UserID = $GLOBAL_USERID;");
    $row = $result->fetch_assoc();
    $isadmin = $row['Admin'];
}


$mail_debug = (isset($_GET['mail_debug']) && $_GET['mail_debug'] === 'true') ? true : false;

// Functions
function send_email($to, $subject, $message, $headers, $params=null){
  $GLOBALS['smtp_server'] = isset($GLOBALS['smtp_server']) ? $GLOBALS['smtp_server'] : 'localhost';
  $GLOBALS['smtp_port'] = isset($GLOBALS['smtp_port']) ? (int)$GLOBALS['smtp_port'] : 587;

  require('SMTPMailer.php');
  $mail = new SMTPMailer($GLOBALS['smtp_server'], $GLOBALS['smtp_port'], 'tls');
  $mail->Auth($GLOBALS['smtp_username'], $GLOBALS['smtp_password']);

  $mail->addTo($to);
  $mail->Subject($subject);
  $mail->Text($message);
  $mail->From('valsedels.admin@piratpartiet.se');

  $send_res = $mail->Send();

  if ($send_res !== true || $GLOBALS['mail_debug'] === true){
    //echo $mail->ShowLog();
  }
}


function does_user_have_responsibilities($conn, $userid){
	$userid = $conn->real_escape_string($userid);
	// Finns det bokningar, finns det valdagshjälte, finns det kommunansvarig?
	return ((int)$conn->query('SELECT * FROM Booking WHERE UserID='.$userid)->num_rows > 0 || (int)$conn->query('SELECT * FROM valdagshjalte WHERE UserID='.$userid)->num_rows > 0 || (int)$conn->query('SELECT * FROM kommunansvarig WHERE UserID='.$userid)->num_rows > 0);
}

function is_user_valdagshjalte($conn, $userid){
	$userid = $conn->real_escape_string($userid);
}

function check_if_lokal_has_received_sedlar($conn, $lokalkod){
	$l = $lokal = get_lokal($conn, $lokalkod);
	$b = $bokning = get_booking_by_lokalkod($conn, $lokalkod);
	return ($l !== null && $b !== null && $l['AntalR'] === $b['AntalR'] && $l['AntL'] === $b['AntalL'] && $l['AntalK'] === $b['AntalK']);
}

function get_bookings_by_userid($conn, $userid){
  $userid = $conn->real_escape_string($userid);
  $result = $conn->query('SELECT * FROM Booking WHERE UserID='.$userid);
  return fetch_all_from_db_result($result);
}

function get_lokaler_by_userid($conn, $userid){
	$userid = $conn->real_escape_string($userid);
	$result = $conn->query('SELECT * FROM Vallokal WHERE LokalKod IN (SELECT LokalID FROM Booking WHERE UserID='.$userid.')');
	$retval = array();
	return fetch_all_from_db_result($result);
}

function get_lokal_by_typ($conn, $typ){
  $typ = strtoupper(substr($conn->real_escape_string($typ), 0, 1));
  if (in_array($typ, ['F', 'V']) === false){
    return [];
  }
  $result = $conn->query('SELECT * FROM Vallokal WHERE Typ="'.$typ.'"');
  return fetch_all_from_db_result($result);
}

function fetch_all_from_db_result($res){
  $retval = [];
  if ($res->num_rows > 0){
    while($data = $res->fetch_assoc())
    $retval[] = $data;
  }
  return $retval;
}

function get_valdagslokaler($conn){
  return get_lokal_by_typ($conn, 'V');
}

function get_fortidsrostningslokaler($conn){
  return get_lokal_by_typ($conn, 'F');
}

function get_all_lokaler($conn){
  $result = $conn->query('SELECT * FROM Vallokal');
  return fetch_all_from_db_result($result);
}

function get_lokaler_oppna_idag_och_imorgon($conn){
  $result = $conn->query('SELECT *, IF(f_oppet_idag OR(v_oppet_idag), 1, 0) oppet_idag, IF(f_oppet_imorgon OR(v_oppet_imorgon), 1, 0) oppet_imorgon FROM (SELECT *, IF(Tider LIKE CONCAT("%", CURDATE(), "%"),1,0) f_oppet_idag, IF(Tider LIKE CONCAT("%", ADDDATE(CURDATE(), 1), "%"),1,0) f_oppet_imorgon, IF(CURDATE()="2022-09-11" AND(Typ="V"),1,0) v_oppet_idag, IF(CURDATE()="2022-09-10" AND(Typ="V"),1, 0) v_oppet_imorgon FROM Vallokal v1 WHERE Tider LIKE CONCAT("%", CURDATE(), "%") OR Tider LIKE CONCAT("%", ADDDATE(CURDATE(), 1) , "%") or Typ = IF(CURDATE()="2022-09-10","V","Janne") OR Typ = IF(CURDATE()="2022-09-11","V","Janne")) t');
  return fetch_all_from_db_result($result);
}

function get_lokaler_oppna_idag($conn){
  $result = $conn->query('SELECT *, IF(f_oppet_idag OR(v_oppet_idag), 1, 0) oppet_idag FROM (SELECT *, IF(Tider LIKE CONCAT("%", CURDATE(), "%"),1,0) f_oppet_idag, IF(CURDATE()="2022-09-11" AND(Typ="V"),1,0) v_oppet_idag FROM Vallokal WHERE Tider LIKE CONCAT("%", CURDATE(), "%") OR Typ = IF(CURDATE()="2022-09-11","V","Janne")) t');
  return fetch_all_from_db_result($result);
}

function get_avklarade_lokaler_by_user($conn, $userid){
  $userid = $conn->real_escape_string($userid);
  $result = $conn->query('select * from (select * from (select UserID, LokalID  from booking where UserId = '.$userid.') b left join vallokal v on v.LokalKod=b.LokalID) t where Klar = "K"');
  return fetch_all_from_db_result($result);
}

function get_ej_avklarade_lokaler_by_user($conn, $userid){
  $userid = $conn->real_escape_string($userid);
  $result = $conn->query('select * from (select * from (select UserID, LokalID from booking where UserId = '.$userid.') b left join vallokal v on v.LokalKod=b.LokalID) t where Klar != "K"');
  return fetch_all_from_db_result($result);
}

function get_booking_by_lokalkod($conn, $lokalkod){
	$lokalkod = $conn->real_escape_string($lokalkod);
  $result = $conn->query('SELECT * FROM Booking WHERE LokalID = "'. $lokalkod . '"');
	if ($result->num_rows === 1){
		return $result->fetch_assoc();
	}
	return null;
}

function get_lokal($conn, $lokalkod){
	$lokalkod = $conn->real_escape_string($lokalkod);
	$result = $conn->query('SELECT * FROM vallokal WHERE LokalKod="'.$lokalkod.'" LIMIT 1');
	if ($result->num_rows === 1){
		return $result->fetch_assoc();
	}
	return null;
}

function set_password_for_user($conn, $userid, $new_password){
  // Använd inte den här om du inte vet vad du gör
  $userid = $conn->real_escape_string($userid);
  $hash = password_hash($conn->real_escape_string($new_password), PASSWORD_DEFAULT);
  $conn->query('UPDATE user SET Passwd="'.$hash.'" WHERE userID='.$userid.' LIMIT 1');
}

function login($conn, $email, $password){
	$email = $conn->real_escape_string($email);
	$password = $conn->real_escape_string($password);
	$result = $conn->query('SELECT * FROM User where mail="' .  $email . '" LIMIT 1');
	if ($result->num_rows !== 1) {
		return 'Kunde inte logga in!<br>Kontrollera dina uppgifter och försök igen!';
	}
	$data = $result->fetch_assoc();
	if (password_verify($password, $data["Passwd"]) === false){
		return 'Kunde inte logga in!<br>Kontrollera dina uppgifter och försök igen!';
	}
	$_SESSION['UserID'] = $data["UserID"];
	$GLOBALS['GLOBAL_USERID'] = $_SESSION['UserID'];
	return true;
}

function boka_valdagshjalte($conn, $userid, $lan, $kommun) {
	if (isset($GLOBAL_USERID) === false){
		$GLOBAL_USERID = $GLOBALS['GLOBAL_USERID'];
	}
	$result = $conn->query("SELECT ID FROM Kommun WHERE LänID = '$lan' AND KommunID = '$kommun'");
	$kommunid = $result->fetch_assoc()["ID"];

	$result = $conn->query("INSERT INTO valdagshjalte VALUES (null, $userid, $kommunid)");
	require("includes/header.php");
	echo "<main class='row'>
			<div class='col-md-8 offset-md-2'>
				<p>Tack! Du är nu uppbokad som valdagshjälte! För att allt ska gå så smidigt som möjligt, hör av dig till din kommunansvarig så snart som möjligt så kan ni stämma av vad som behöver göras. <a href='mina_lokaler.php'>Gå vidare för att se kontaktuppgifter</a></p>
			</div>
		</main>";
	require("includes/footer.php");
}

function boka_kommunansvarig($conn, $userid, $lan, $kommun) {
	if (isset($GLOBAL_USERID) === false){
		$GLOBAL_USERID = $GLOBALS['GLOBAL_USERID'];
	}

	$result = $conn->query("SELECT ID FROM Kommun WHERE LänID = '$lan' AND KommunID = '$kommun'");
	$kommunid = $result->fetch_assoc()["ID"];

	$result = $conn->query("INSERT INTO kommunansvarig VALUES (null, $userid, $kommunid, null, null)");
	require("includes/header.php");
	echo "<main class='row'>
			<div class='col-md-8 offset-md-2'>
				<p>Tack! Du är nu uppbokad som kommunansvarig! För att allt ska gå så smidigt som möjligt, ta kontakt med andra aktivister i din kommun så fort dom dyker upp på listan. Du ser listan om du klickar på \"Mina ansvarsområden\". <a href='mina_lokaler.php'>Gå vidare</a></p>
			</div>
		</main>";
	require("includes/footer.php");
}


function boka_lokal($conn, $userid, $lokalkod) {
	$res = $conn->query('SELECT * FROM Booking WHERE LokalID = "' . $lokalkod . '";');
	if ($res->num_rows > 0 ) {
		header('Location: mina_lokaler.php');
		return;
	}
	$booking = 'INSERT INTO Booking (userID, LokalID) VALUES (' . $userid . ',"' .  $lokalkod . '")';
	$result = $conn->query($booking);
	//echo $booking . "<br>";
	//echo $result . "<br>";

	//Uppdatera lokalstatus
	$uls ='UPDATE vallokal SET status="B" WHERE lokalkod="' . $lokalkod . '"';
	//echo $lk . "<br>";
	$re = $conn->query($uls);


	$result = $conn->query('SELECT * FROM vallokal where LokalKod="' . $lokalkod . '"');
	$lokal = $result->fetch_assoc();

	$redirect_url = '';
/*
	if ($lokal["Typ"] == "F") {
		$redirect_url = 'fget_lokaler.php?typ=f&lan=' . $lan . '&kommun=' . $kommun;
	} else {
	    $redirect_url = 'fget_lokaler.php?typ=v&lan=' . $lan . '&kommun=' . $kommun;
	}
	//echo $lan . " " . $kommun . "<br>";
	*/
	header('Location: mina_lokaler.php');
}

function sendSMS ($sms) {
  global $sms_username;
  global $sms_token;
  $username = $sms_username;
  $password = $sms_token;

//  $sms['dryrun'] = 'yes'; // todo: remove


        $telefon = preg_replace('/[^0-9]/', '', $sms['to']);
        if(strpos($telefon, '00') === 0) {
                $telefon = substr_replace($telefon, '+', 0, 2);
        } else if(strpos($telefon, '46') === 0) {
                $telefon = substr_replace($telefon, '+46', 0, 2);
        } else if(strpos($telefon, '0') === 0) {
                $telefon = substr_replace($telefon, '+46', 0, 1);
        } else if(strpos($telefon, '7') === 0) {
                $telefon = substr_replace($telefon, '+467', 0, 1);
        }
	$sms['to'] = $telefon;


  $context = stream_context_create(array(
    'http' => array(
      'method' => 'POST',
      'header'  => 'Authorization: Basic '.
                  base64_encode($username.':'.$password). "\r\n".
                  "Content-type: application/x-www-form-urlencoded\r\n",
      'content' => http_build_query($sms),
      'timeout' => 10
  )));
  $response = file_get_contents("https://api.46elks.com/a1/sms",
    false, $context);

  if (!strstr($http_response_header[0],"200 OK"))
    return $http_response_header[0];
  return $response;
}
//$sms = array(
//  "from" => "PHPElk",   /* Can be up to 11 alphanumeric characters */
//  "to" => "+46700000000",  /* The mobile number you want to send to */
//  "message" => "Bring a sweater, it's cold outside!",
//);
//echo sendSMS($sms);
