<?php
require("includes/open_database.php");


$lokalkod = $_GET['lokal'];
$lokal_data = get_lokal($conn, $lokalkod);
$bokning = get_booking_by_lokalkod($conn, $lokalkod);

if ($lokal_data !== null && $bokning !== null && $bokning['UserID'] === $GLOBAL_USERID){
	$result = $conn->query('SELECT * FROM vallokal where LokalKod="' . $conn->real_escape_string($lokalkod) . '"');
	$lokal = $result->fetch_assoc();
	
	$conn->query('UPDATE vallokal SET Status = "B" WHERE LokalKod="' . $conn->real_escape_string($lokalkod) . '"');
}
header('Location: mina_lokaler.php');
