<?php
require("includes/open_database.php");
require("includes/jwt.php");
/*

A "password reset" is the equivalent of getting a carte-blanche Login, so let's
just log the person in and redirect them to the password change form if they
provided the correct token with a correct payload.

*/

// Already logged in people doesn't need to get logged in again
if ($GLOBAL_USERID > 0){
  header('Location: /');
}

function validate_password_reset_payload_format($payload){
  if (is_array($payload) === false || isset($payload['u']) === false || isset($payload['h']) === false || isset($payload['exp']) === false){
    return false;
  }
  if ((int)$payload['u'] > 0 && ctype_xdigit($payload['h']) && strlen($payload['h']) === 8 &&  is_int($payload['exp']) === true && $payload['exp'] > 0){
    return true;
  }
  return false;
}

$token_status = 'invalid';

$token = isset($_GET['t']) ? $_GET['t'] : null;

$token_valid_format = validate_jwt_format($token);
if ($token_valid_format){ // General format check
  $token_payload = get_jwt_payload($token);
  if (validate_password_reset_payload_format($token_payload)){
    $userid = $conn->real_escape_string($token_payload['u']);
    $result = $conn->query('SELECT * FROM User WHERE UserID="'.$userid.'"');
    if ($result->num_rows === 1){
      $user = $result->fetch_assoc();
      $h = substr(hash('sha256', json_encode($user)),0,8);
      $token_secret = hash('sha256', $user['Passwd']);
      $token_validity = validate_jwt($token, $token_secret);
      if($h === $token_payload['h']){ // Hash value check
        if ($token_payload['exp'] > time()){ // Expiration check
            if ($token_validity === true){ // Signature check
              $token_status = ($token_validity === true) ? 'valid' : $token_status;
              $_SESSION['UserID'] = $user["UserID"];
              $GLOBALS['GLOBAL_USERID'] = $_SESSION['UserID'];
              header('Location: change_password.php');
              exit();
            } else {
              // Ogiltig signatur, wut?
            }
        } else {
          // Utgången JWT
          $token_status = 'expired';
        }
      } else {
        // Mixtrat med hashvärdet (why?)
      }
    } else {
      // Hittade ingen (eller flera) användare
    }
  } else {
    // Format error
  }
} else {
  //Trasig tokendata
}

$token_message = ($token_status === 'invalid') ? 'Ogiltig återställningskod. Vänligen kontrollera att du fått med hela länken.' : '';
$token_message = ($token_status === 'expired') ? 'Denna token har gått ut, vänligen gör en ny lösenordsåterställning.' : $token_message;

require('includes/header.php');
?>
<main class="row">
  <div class="col-md-8 offset-md-2">
    <div class="alert alert-danger" role="alert">
      <?php echo htmlspecialchars($token_message); ?>
    </div>
  </div>
</main>
