<?php
require("includes/open_database.php");
require("includes/header.php");

$userid = $conn->real_escape_string($GLOBAL_USERID);
$kommunid = $conn->real_escape_string($_GET['kommunid']);

?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item"><a href="mina_lokaler.php">Mina ansvarsområden</a></li>
	<li class="breadcrumb-item active" aria-current="page">Valdagshjältar</li>
      </ol>
    </nav>
  </div>
</div>
<?php

 $result = $conn->query("SELECT * FROM kommunansvarig where userid = '$userid' and kommunid = '$kommunid'");
 if ($result->num_rows > 0) {
	$r2 = $conn->query("SELECT * FROM Kommun where ID = '$kommunid'");
	$kommunrow = $r2->fetch_assoc();
	?>
	<script>$("#menuitem-kommunansvarig").css("display", "block");</script>
	<div class="row">
	  <div class="col-md-8 offset-md-2">
		<h2 id="valdagshjalte">Valdagshjältar</h2>
		    <p>En valdagshjälte är någon som vill göra lite extra, någon som inte bara kan tänka sig att fara till den närmaste vallokalen, utan som faktiskt kan tänka sig att göra en insats där det behövs.</p>
		    <p>valdagshjältar sammarbetar med dig som kommunansvarig för att på valdagen (och när förtidsröstningen börjar) se vilka lokaler som inte någon annan lägger ut valsedlar i. Sedan så åker hen till så många av dom som möjligt.</p>
		    <p>Du ser kontaktuppgifter till dom som anmält sig för att vara valdagshjältar nedan. Ta kontakt med dom så snart som möjligt för att stämma av.</p>

	<?php
	while($row = $result->fetch_assoc()) {
		$sql = "SELECT * FROM User WHERE UserID IN (SELECT userid FROM valdagshjalte WHERE kommunid = '$kommunid')";
		$aktivistresult = $conn->query($sql);
		$print_stupid_ass_backwards_stuff = true;
		while($userrow = $aktivistresult->fetch_assoc()) {
			$print_stupid_ass_backwards_stuff = false;

			echo "<p>".htmlspecialchars($userrow['namn'])." ".htmlspecialchars($userrow['efternamn'])."<br>".htmlspecialchars($userrow['adress'])."<br>".htmlspecialchars($userrow['postadress'])."</p>";
			echo "<p>Telefon: ".htmlspecialchars($userrow['telefon'])."</p><p>Mail: <a href='mailto:" .htmlspecialchars($userrow['mail'])."'>".htmlspecialchars($userrow['mail'])."</a></p>";
			echo '<hr>';
		}
	}
	if ($print_stupid_ass_backwards_stuff === true){
		echo "<p>Det finns för närvarande inga valdagshjältar i $kommunrow[Namn] :(</p>";
	}
	echo '</div></div>';
 }
require("includes/footer.php");
