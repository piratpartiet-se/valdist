<?php
require("includes/open_database.php");
require("includes/header.php");

$userid = $conn->real_escape_string($GLOBAL_USERID);
$kommunid = $conn->real_escape_string($_GET['kommunid']);

?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item"><a href="mina_lokaler.php">Mina ansvarsområden</a></li>
	<li class="breadcrumb-item active" aria-current="page">Lämna valsedlar</li>
      </ol>
    </nav>
  </div>
</div>
<?php

 $result = $conn->query("SELECT * FROM kommunansvarig where userid = '$userid' and kommunid = '$kommunid'");
 if ($result->num_rows > 0) {
	$r2 = $conn->query("SELECT * FROM Kommun where ID = $kommunid");
	$kommunrow = $r2->fetch_assoc();
	?>
	<script>$("#menuitem-kommunansvarig").css("display", "block");</script>
	<div class="row">
	  <div class="col-md-8 offset-md-2">
		<h2 id="kommunansvarig">Du ska lämna valsedlar till följande aktivister i <?php echo "$kommunrow[Namn]";?></h2>
	    <p>Se till att lämna rätt mängd valsedlar, annars kommer dom inte räcka! Tips: Använd en våg. 100 valsedlar väger 130 gram. Alt om du inte har en våg så är 100 valsedlar ca 1 cm</p>

	<?php
	while($row = $result->fetch_assoc()) {
		$sql = "SELECT * FROM User WHERE userid IN (SELECT UserID FROM Booking where LokalID IN (select LokalKod from vallokal right join (select KommunID, LänID from Kommun where ID in (select kommunid from kommunansvarig where userid = $userid)) t1 on KommunID = KommunKod and LänID = LanKod));";
		$aktivistresult = $conn->query($sql);
		$print_stupid_ass_backwards_stuff = true;
		while($userrow = $aktivistresult->fetch_assoc()) {
			$riksdagsvalsedlarresult = $conn->query("SELECT SUM(vallokal.AntalR) AntalR FROM Booking LEFT JOIN vallokal ON LokalID = lokalkod WHERE Booking.AntalR = 0 AND UserID = $userrow[UserID];");
			$riksdagsvalsedlar = round($riksdagsvalsedlarresult->fetch_assoc()['AntalR']/10)*10;
			if ((int)$riksdagsvalsedlar === 0){ continue;} // Vansinne att göra saker i den här ordningen
			$print_stupid_ass_backwards_stuff = false;
			echo "<p>".htmlspecialchars($userrow['namn'])." ".htmlspecialchars($userrow['efternamn'])."<br>".htmlspecialchars($userrow['adress'])."<br>".htmlspecialchars($userrow['postadress'])."</p>";
			echo "<p>Telefon: ".htmlspecialchars($userrow['telefon'])."</p><p>Mail: <a href='mailto:" .htmlspecialchars($userrow['mail'])."'>".htmlspecialchars($userrow['mail'])."</a></p>";
			$sql = "SELECT Namn, AntalL FROM (SELECT LanKod,  SUM(vallokal.AntL) AntalL FROM Booking LEFT JOIN vallokal ON LokalID = lokalkod WHERE Booking.AntalL = 0 AND UserID = $userrow[UserID] GROUP BY LanKod HAVING AntalL > 0) t1 LEFT JOIN Län ON LanKod = LänID;";
			$lanresult = $conn->query($sql);
			$landstingsvalsedlar = "";
			while($lanrow = $lanresult->fetch_assoc()) {
				$antalLan = round($lanrow['AntalL']/10)*10;
				$landstingsvalsedlar .= ", $antalLan Landstingsvalsedlar för $lanrow[Namn]";
			}

			$sql = "SELECT Namn, AntalK FROM (SELECT LanKod, KommunKod, SUM(vallokal.AntalK) AntalK FROM Booking LEFT JOIN vallokal ON LokalID = lokalkod WHERE Booking.AntalK = 0 AND UserID = $userrow[UserID] GROUP BY KommunKod, LanKod HAVING AntalK > 0) t1 LEFT JOIN Kommun ON LanKod = LänID AND KommunKod = KommunID;";
			$kommunresult = $conn->query($sql);
			$kommunvalsedlar = "";
			while($kommunrow = $kommunresult->fetch_assoc()) {
				$antalKommun = round($kommunrow['AntalK']/10)*10;
				$kommunvalsedlar .= ", $antalKommun Kommunvalsedlar för $kommunrow[Namn]";
			}
			echo "<p>Du ska ge " . htmlspecialchars($userrow['namn']) . " " . $riksdagsvalsedlar ." riksdagsvalsedlar $landstingsvalsedlar$kommunvalsedlar</p>";
			echo '<a class="btn btn-pirate" href="save_lamna_valsedlar.php?userid='.$userrow['UserID'].'&kommunid='.htmlspecialchars($_GET['kommunid']).'" role="button" style="margin: 5px"><i class="fas fa-caret-right"></i> Markera som överlämnade <i class="fas"></i></a>';
			echo '<hr>';
		}
	}
	if ($print_stupid_ass_backwards_stuff === true){
		echo '<p>Det finns för närvarande inga aktivister som behöver få valsedlar.</p>';
	}
	echo '</div></div>';
	echo '<hr>';
 }
require("includes/footer.php");
