<?php
require("includes/open_database.php");

$first_name = $conn->real_escape_string($_POST["first_name"]);
$last_name = $conn->real_escape_string($_POST["last_name"]);
$street_adress = $conn->real_escape_string($_POST["street_address"]);
$post_address = $conn->real_escape_string($_POST["post_address"]);
$phone_number = $conn->real_escape_string($_POST["phone_number"]);
$email_address = $conn->real_escape_string($_POST["email_address"]);

$conn->query('UPDATE User SET mail="' . $email_address . '", namn="' . $first_name . '", efternamn="' . $last_name . '", adress="' . $street_adress . '", postadress="' . $post_address . '", telefon="' . $phone_number . '" WHERE UserID=' . $conn->real_escape_string($GLOBAL_USERID));

$conn->close();

//echo $lan . " " . $kommun . "<br>";
header('Location: /');
exit();
?>
