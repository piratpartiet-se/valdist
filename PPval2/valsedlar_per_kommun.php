<?php
require("includes/open_database.php");
require("includes/header.php");


$sql = "select Namn, AntalR as RiksFortid, AntalL as LandstingFortid, AntalK as KommunFortid, AntalRV as RiksValdag, AntalLV as LandstingValdag, AntalKV as KommunValdag, AntalR+AntalRV as Riks, AntalL+AntalLV as Landsting, AntalK+AntalKV As Kommun from (select * from (select LanKod, KommunKod, sum(AntalR) as AntalR, sum(AntL) as AntalL, sum(AntalK) as AntalK from vallokal where Typ = 'F' group by LanKod, KommunKod) tl left join (select LanKod, KommunKod, sum(AntalR) as AntalRV, sum(AntL) as AntalLV, sum(AntalK) as AntalKV from vallokal where Typ != 'F' group by LanKod, KommunKod) t2 using (LanKod, KommunKod)) t3 left join Kommun on LanKod = LänID and KommunKod = KommunID";

echo '<div class="row">
  <div class="col-md-8 offset-md-2">';

echo '<h1>Antal Valsedlar per Kommun</h1>';
echo '<table class="table table-bordered table-hover">';
echo '<thead><tr>
		<th>Kommun</th>
		<th class="table-warning">Riksdag förtid</th>
		<th class="table-primary">Landsting förtid</th>
		<th>Kommun förtid</th>
		<th class="table-warning">Riksdag valdag</th>
		<th class="table-primary">Landsting valdag</th>
		<th>Kommun valdag</th>
		<th class="table-warning">Riksdag total</th>
		<th class="table-primary">Landsting total</th>
		<th>Kommun total</th>

	</tr></thead>';
$result = $conn->query($sql);

$classes = ['', 'table-warning', 'table-primary'];
while($row = $result->fetch_assoc()) {
	$i = 0;
	echo '<tr>';
	foreach ($row as $val) {
		echo "<td class='".$classes[$i%3]."'>$val</td>";
		$i++;
	}
	echo '</tr>';
}
echo '</div></div>';

require("includes/footer.php");
