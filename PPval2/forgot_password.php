<?php
require("includes/open_database.php");
require("includes/jwt.php");

$debug_arrs = array();

// Already logged in people doesn't need to get their password reset
if ($GLOBAL_USERID > 0){
  header('Location: /');
}

function create_reset_code($dbu){
  /* Ta en assoc-array representerandes usern, skapa en ny assoc-array innehållandes:
  u = userid,
  h = åtta första tecknen av user-representerande assoc-arrayen som JSON-sträng,
  exp = utgångstid för återställningslänken (24 timmar)

  Generera en JWT med datat från tidigare nämnda assoc-array, där secreten är en hashning av
  lösenordsfältet i databasen (Vilket invaliderar alla tidigare tokens då den inte kommer matcha,
  samt låter aktuell token vara giltig fram tills lösenord bytts). Pga. PHPs bcrypt-klägg så byts
  salten ut även om användaren skulle råka vara enormt klantig och byta till samma lösenord som
  redan var satt så kommer

  Man kan argumentera för att nyckeln 'h' inte behövs, men det är inte mycket extra data och
  lite extra förvirring för någon som försöker titta på det gör inget.
  */
  $pl = array('u'=>$dbu['UserID'],'h'=>substr(hash('sha256',json_encode($dbu)),0,8),'exp'=>time()+(3600 * 24));
  return generate_jwt($pl, hash('sha256', $dbu['Passwd']));
}

$email_address = isset($_POST['email_address_pw_reset']) ? $conn->real_escape_string($_POST['email_address_pw_reset']) : null;
if ( $email_address !== null ){
  if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) { // Inbyggda funktioner för backend-validering? *surprised pikachu.png*
    $err = "Ogiltig mailadress";
  }
  if (strlen($email_address) === 0){
    $err = "Du måste fylla i en e-postadress";
  }
}

if (isset($err) === false && $email_address !== null){
  $result = $conn->query("SELECT * FROM User WHERE mail='".$email_address."'");
  if ($result->num_rows === 1){
    $debug_arrs[] = 'dbuser found';
    $usr = $result->fetch_assoc();
    $reset_code = create_reset_code($usr);
    $debug_arrs[] = 'user code created';
    $mail_address = $usr['mail'];
    $subj = "Begäran om lösenordsåterställning på valsedel.piratpartiet.se";
    $head = "From:noreply@valsedel.piratpartiet.se \r\n";
    $plain_text = "Hej!\n
    Någon har begärt en lösenordsåterställning för ditt konto på valsedlar.piratpartiet.se.\n\n
    Om det inte var du, vänligen kontakta en administratör på sajten.\n\n
    Annars, använd följande länk för att återställa ditt lösenord:\n\n
    https://valsedel.piratpartiet.se/reset_password.php?t=".urlencode($reset_code)."\n\n
    (Detta är ett automatiskt meddelande och du kan inte svara på det)";

    send_email($mail_address, $subj, $plain_text, $head);
    $debug_arrs[] = 'sent email';
  } else {
    $debug_arrs[] = 'nodbuser';
  }
  $processed = true;
}
require('includes/header.php');
if (isset($_GET['mail_debug']) && $_GET['mail_debug'] === 'true'){
  echo '<!-- Debugging enabled -->';
  foreach($debug_arrs as $msg){
    // echo '<!-- '.$msg.' -->';
  }
  echo '<!-- End of debug log -->';
}

?>

<div class="row">
  <div class="col-md-8 offset-md-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Startsida</a></li>
        <li class="breadcrumb-item">Glömt Lösenord</li>
      </ol>
    </nav>
  </div>
</div>
<main>
  <div class="col-md-8 offset-md-2">
    <h2>Glömt Lösenord</h2>
    <p>Det var ju lite klantigt, men det går att fixa. Fyll i din e-postadress nedan så kommer ett mail med instruktioner för återställning.</p>
    <hr>
  </div>
  <?php
  if (isset($err)){
    // Rita ett felmeddelande
    echo '<div class="col-md-8 offset-md-2">';
    echo '<div class="alert alert-danger" role="alert">Fel: '.$err.'</div>';
    echo '</div><div class="col-md-8 offset-md-2">';
  }
  if (isset($processed) === true && $processed === true){
    // Rita ett rätt-meddelande
    echo '<div class="col-md-8 offset-md-2">';
    echo '<div class="alert alert-success" role="alert">Håll utkik efter ett mail med återställningslänk! Observera att den gäller i 24 timmar!</div>';
    echo '</div><div class="col-md-8 offset-md-2">';
    $email_address = '';
  }
  ?>
  <div class="col-md-8 offset-md-2">
    <?php if (isset($processed) === false || $processed === false){ ?>
    <form method="post">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="email-address">E-postadress</label>
          <input type="email" class="form-control" id="email-address-pw-reset" name="email_address_pw_reset" placeholder="" value="<?php echo htmlspecialchars($email_address) === '' ? '' : $email_address; ?>">
        </div>
      </div>
      <hr>
      <button id="tackDinKommentar" type="submit" class="btn btn-primary"><i class="fas fa-caret-right"></i> Gå vidare <i class="fas fa-clipboard-list"></i></button>
    </div>
  </form>
</main>
<script>
$('#tackDinKommentar').on('click', function(ev) {
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    var mail = $('#email-address-pw-reset').val();
    var res = re.test(mail);

    if ( ! res ) {
      alert('Fyll i din mailaddress');
      return false;
    }
	});
</script>
<?php } else {
  // Om vi är klara så kastar vi tillbaks användaren till inloggningssidan
  //echo '<script>setTimeout(function(){window.location.href = "/";}, 10000);</script>'; //Eller Näe
}
