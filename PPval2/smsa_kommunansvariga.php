<?php
require("includes/open_database.php");

set_time_limit(7200);
ini_set('max_execution_time',7200);


if (!$isadmin) {
	header("Location: /");
	exit;
}

require("includes/header.php");

echo '<main class="row">  <div class="col-md-8 offset-md-2">';


if(isset($_GET['done']) && $_GET['done'] == 1) {
	echo '<h1>Klart!</h1>';
}

//$result = $conn->query("SELECT concat(namn, ' ', efternamn) namn, concat(adress, ' ', postadress) address, mail, telefon FROM User WHERE UserID IN (select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod  = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID GROUP BY kommunansvarig.UserID);");
//$result = $conn->query("select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID;");

//$result = $conn->query("select KommunKod, LanKod from Booking LEFT JOIN vallokal ON LokalID = LokalKod WHERE Booking.AntalR = 0 group by KommunKod, LanKod;");

$result = $conn->query("select * from kommunansvarig;");
$numbers = [];
echo '<table class="table table-bordered table-hover">';

while($row = $result->fetch_assoc()) {
	$result2 = $conn->query("SELECT * FROM Booking WHERE Booking.LokalID IN (select LokalKod from Kommun RIGHT JOIN vallokal ON KommunID = KommunKod AND LänID = LanKod WHERE Kommun.ID = $row[kommunid]) and Booking.UserID != $row[userid] AND Booking.AntalR = 0;");
	while($row2 = $result2->fetch_assoc()) {
		$result3 = $conn->query("SELECT * FROM User WHERE UserID = $row[userid]");
		while($row3 = $result3->fetch_assoc()) {
			$numbers[$row3['telefon']] = $row3['telefon'];
			echo '<tr><td>'.htmlspecialchars($row['kommunid']).'</td><td>' . htmlspecialchars($row3['namn']) . '</td><td>' . htmlspecialchars($row3['address']) . '</td><td>' . htmlspecialchars($row3['mail'])  . '</td><td>' . htmlspecialchars($row3['telefon']) . '</td></tr>';
		}
	}
}

echo '</table>';

echo 'Totalt kommer ' . count($numbers) . ' SMS gå ut. ';
echo '<a href="smsa_kommunansvariga.php?send=1">Skicka</a>';

echo '</div></main>';

if(isset($_GET['send']) && $_GET['send'] == 1) {
	foreach($numbers as $i => $number) {
//		file_put_contents($log, "$i: $number\n", FILE_APPEND);
		echo "$i: $number<br>";
		$sms = array(
		  "from" => "PP",   /* Can be up to 11 alphanumeric characters */
		  "to" => $number,  /* The mobile number you want to send to */
		  "message" => "Piratpartiet: Hej kommunansvarig! Det finns aktivister som skrivit upp sig på att hjälpa till i din kommun! Ta kontakt med dom för att ge dom valsedlar. När du lämnat över valsedlar, markera detta på valsedelssidan. Se https://valsedel.piratpartiet.se/mina_lokaler.php#kommunansvarig",
		);
		sleep(1);
		$res = sendSMS($sms);
		echo $res;
		flush();
	}
	header("Location: smsa_kommunansvariga.php?done=1");

}

exit;
