<?php
require("includes/open_database.php");
require("includes/header.php");

function format_decimal_text($decimal_string){
  return rtrim(rtrim($decimal_string, '0'), '.');
}
?>

<main class="row">
  <div class="col-md-8 offset-md-2">
<p>Piratpartiet behöver själva lägga ut valsedlar i alla vallokaler -- och vi behöver din hjälp!</p>
<p>Börja med att välja län och sedan kommun. När du gjort det kan du välja vilka vallokaler i kommunen du kan lägga ut i.</p>
<p>Du får tag på valsedlar genom att kontakta din kommunansvarig. Kontaktuppgifter ser du efter du har bokat upp dig på en vallokal. Om det inte finns någon kommunansvarig så har du möjlighet att få valsedlar skickade till dig.</p>
    <?php
    $result = $conn->query("SELECT * FROM Län order by namn");
    $result = $conn->query("select *, ROUND((1-(vObokade/vTotal))*100, 2) vTackning, ROUND((1-(fObokade/fTotal))*100, 2) fTackning from
	(select LanKod, SUM(vTotal) vTotal, SUM(vObokade) vObokade, SUM(fTotal) fTotal, SUM(fObokade) fObokade from (
		select *, if(Typ='V', total, 0) as vTotal, if(Typ='V', obokade, 0) as vObokade, if(Typ='F', total, 0) as fTotal, if(Typ='f', obokade, 0) as fObokade from (
			select KommunKod, LanKod, Typ, LokalKod, VoterCount as total, if(Status=null or(lower(Status)='o') or(Status=''), VoterCount, 0) as obokade, if(lower(Status)='b', VoterCount, 0) as bokade, if(lower(Status)='k', VoterCount, 0) as klara from vallokal
		) d
	) d group by LanKod order by LanKod) d
left join Län on Län.LänID = d.LanKod ORDER BY Namn"); // :))))))))))))
    if ($result->num_rows > 0) {
        echo '<table class="table table-bordered table-hover">';
        echo '<thead><tr><th>Län <i class="fas fa-globe-africa"></i></th><th>Ca. antal röstberättigade <i class="fas fa-check"></i></th><th>Täckningsgrad</th><th>Täckningsgrad förtid</th></tr></thead><tbody>';
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo '<tr><td><a href="fget_kommuner.php?lan=' . $row["LänID"] . '">' . $row["Namn"] . '</a></td>';
            echo '<td>' . $row["Röstb"] . '</td>';
            echo '<td>' . htmlspecialchars(format_decimal_text($row['vTackning'])) . '%</td>';
            echo '<td>' . htmlspecialchars(format_decimal_text($row['fTackning'])) . '%</td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
    } else {
        echo '<div class="alert alert-warning" role="alert">Vi hittade tyvärr inga län i vår databas. Vänligen kontakta ansvarig.</div>';
    }

    $conn->close();
    ?>
  </div>
</main>

<?php require("includes/footer.php"); ?>
