<?php
require("includes/open_database.php");

if (!$isadmin) {
	header("Location: /");
	exit;
}

require("includes/header.php");

$lan = isset($_GET['lan']) ? ( 'where Kommun.LänID = "' . $conn->real_escape_string($_GET['lan']) . '"') : '';
$sql = "select Kommun.Namn as kommun, concat(User.namn, ' ', efternamn) kommunansvarig, adress, postadress as postort, telefon, mail, Kommun.Röstb rostberattigade, null as fortid, null as tackning_fortid, null as valdag, null as tackning_valdag from Kommun left join kommunansvarig on Kommun.KommunID = kommunansvarig.kommunid left join User ON kommunansvarig.userid = User.UserID $lan order by kommun";
//$sql = "select Namn, AntalR as RiksFortid, AntalL as LandstingFortid, AntalK as KommunFortid, AntalRV as RiksValdag, AntalLV as LandstingValdag, AntalKV as KommunValdag, AntalR+AntalRV as Riks, AntalL+AntalLV as Landsting, AntalK+AntalKV As Kommun from (select * from (select LanKod, KommunKod, sum(AntalR) as AntalR, sum(AntL) as AntalL, sum(AntalK) as AntalK from vallokal where Typ = 'F' group by LanKod, KommunKod) tl left join (select LanKod, KommunKod, sum(AntalR) as AntalRV, sum(AntL) as AntalLV, sum(AntalK) as AntalKV from vallokal where Typ != 'F' group by LanKod, KommunKod) t2 using (LanKod, KommunKod)) t3 left join Kommun on LanKod = LänID and KommunKod = KommunID";


echo '<div class="row">
  <div class="col-md-8 offset-md-2">';

?>
<h2>Kommunansvariga per län</h2>
<a href="admin_lansoversikt.php?lan=1">Stockholms län</a><br>
<a href="admin_lansoversikt.php?lan=3">Uppsala län</a><br>
<a href="admin_lansoversikt.php?lan=4">Södermanlands län</a><br>
<a href="admin_lansoversikt.php?lan=5">Östergötlands län</a><br>
<a href="admin_lansoversikt.php?lan=6">Jönköpings län</a><br>
<a href="admin_lansoversikt.php?lan=7">Kronobergs län</a><br>
<a href="admin_lansoversikt.php?lan=8">Kalmar län</a><br>
<a href="admin_lansoversikt.php?lan=9">Gotlands län</a><br>
<a href="admin_lansoversikt.php?lan=10">Blekinge län</a><br>
<a href="admin_lansoversikt.php?lan=12">Skåne län</a><br>
<a href="admin_lansoversikt.php?lan=13">Hallands län</a><br>
<a href="admin_lansoversikt.php?lan=14">Västra Götalands län</a><br>
<a href="admin_lansoversikt.php?lan=17">Värmlands län</a><br>
<a href="admin_lansoversikt.php?lan=18">Örebro län</a><br>
<a href="admin_lansoversikt.php?lan=19">Västmanlands län</a><br>
<a href="admin_lansoversikt.php?lan=20">Dalarnas län</a><br>
<a href="admin_lansoversikt.php?lan=21">Gävleborgs län</a><br>
<a href="admin_lansoversikt.php?lan=22">Västernorrlands län</a><br>
<a href="admin_lansoversikt.php?lan=23">Jämtlands län</a><br>
<a href="admin_lansoversikt.php?lan=24">Västerbottens län</a><br>
<a href="admin_lansoversikt.php?lan=25">Norrbottens län</a><br>
<a href="admin_lansoversikt.php">Hela Sverige</a><br>

<h2>Antal valsedlar som ska skickas till varje kommun</h2>
<a href="valsedlar_per_kommun.php">lista</a>

<h2>Valsedlar som behöver bokas frakt för</h2>
<a href="admin_valsedelsutskick.php">lista</a>

<h2>Valsedlar som behöver packas</h2>
<a href="admin_valsedelspack.php">lista</a>

<h2>SMSa kommunansvariga (bara för mlg)</h2>
<a href="smsa_kommunansvariga.php">SMSa</a>

<h2>SMSa aktivister (bara för mlg)</h2>
<a href="smsa_aktivister.php">SMSa</a>

<h2>Lista alla</h2>
<a href="admin_lista_users.php">SMSa</a>

<h2>Alla valdagshjältar</h2>
<a href="admin_lista_valdagshjalte.php">SMSa</a>

<?php

echo '</div></div>';

require("includes/footer.php");
