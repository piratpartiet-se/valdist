<?php
require("includes/open_database.php");

if (!$isadmin) {
        header("Location: /");
	exit;
}

require("includes/header.php");

$lan = isset($_GET['lan']) ? ( 'where Kommun.LänID = "' . $conn->real_escape_string($_GET['lan']) . '"') : '';
$sql = "select Kommun.Namn as kommun, concat(User.namn, ' ', efternamn) kommunansvarig, adress, postadress as postort, telefon, mail, Kommun.Röstb rostberattigade, null as fortid, null as tackning_fortid, null as valdag, null as tackning_valdag, Kommun.KommunID as _kommunid, Kommun.LänID as _lanid from Kommun left join kommunansvarig on Kommun.ID = kommunansvarig.kommunid left join User ON kommunansvarig.userid = User.UserID $lan order by kommun";
//$sql = "select Namn, AntalR as RiksFortid, AntalL as LandstingFortid, AntalK as KommunFortid, AntalRV as RiksValdag, AntalLV as LandstingValdag, AntalKV as KommunValdag, AntalR+AntalRV as Riks, AntalL+AntalLV as Landsting, AntalK+AntalKV As Kommun from (select * from (select LanKod, KommunKod, sum(AntalR) as AntalR, sum(AntL) as AntalL, sum(AntalK) as AntalK from vallokal where Typ = 'F' group by LanKod, KommunKod) tl left join (select LanKod, KommunKod, sum(AntalR) as AntalRV, sum(AntL) as AntalLV, sum(AntalK) as AntalKV from vallokal where Typ != 'F' group by LanKod, KommunKod) t2 using (LanKod, KommunKod)) t3 left join Kommun on LanKod = LänID and KommunKod = KommunID";


echo '<div class="row">
  <div class="col-md-8 offset-md-2">';

echo '<h1>Översikt över kommuner i länet</h1>';
echo '<p>(färgerna är irrelevanta, har inget med kommun/landsting/riks att göra)</p>';
echo '<table class="table table-bordered table-hover">';
echo '<thead><tr>
		<th>Kommun</th>
		<th class="table-warning">Kommunansv</th>
		<th class="table-primary">Address</th>
		<th>Postort</th>
		<th class="table-warning">Telefon</th>
		<th class="table-primary">Mail</th>
		<th>Röstberättigade</th>

		<th class="table-warning"># förtid</th>
		<th class="table-primary">Täckning förtid</th>
		<th># valdag</th>

		<th class="table-warning">Täckning valdag</th>
	</tr></thead>';
$result = $conn->query($sql);

$classes = ['', 'table-warning', 'table-primary'];
while($row = $result->fetch_assoc()) {

	$sql = "select sum(1) as c, sum(vallokal.AntalR) as tot, sum(if(Booking.AntalR is null, 0, vallokal.AntalR)) as booked from vallokal left join Booking on vallokal.LokalKod = Booking.LokalID where KommunKod = $row[_kommunid] and LanKod = $row[_lanid] and Typ = 'F';";
        $result1 = $conn->query($sql);
        $row1 = $result1->fetch_assoc();
	$row['fortid'] = $row1['c'];
	$row['tackning_fortid'] = round($row1['booked'] / $row1['tot'] * 100, 1) . "%";


	$sql = "select sum(1) as c, sum(vallokal.AntalR) as tot, sum(if(Booking.AntalR is null, 0, vallokal.AntalR)) as booked from vallokal left join Booking on vallokal.LokalKod = Booking.LokalID where KommunKod = $row[_kommunid] and LanKod = $row[_lanid] and Typ = 'V';";
        $result1 = $conn->query($sql);
        $row1 = $result1->fetch_assoc();
	$row['valdag'] = $row1['c'];
	$row['tackning_valdag'] = round($row1['booked'] / $row1['tot'] * 100, 1) . "%";

	$i = 0;
	echo '<tr>';
	foreach ($row as $key => $val) {
		if ($key[0] == '_' ) {
			continue;
		}
		if($i == 0){
			$val = "<a href='admin_kommunoversikt.php?lan=$row[_lanid]&kommun=$row[_kommunid]'>$val</a>";
		}
		echo "<td class='".$classes[$i%3]."'>$val</td>";
		$i++;
	}
	echo '</tr>';
}
echo '</div></div>';

require("includes/footer.php");
