<?php
require("includes/open_database.php");
//require("includes/header.php");

$lokalkod= $conn->real_escape_string($_GET["lokalkod"]);

$first_name = $last_name = $street_adress = $post_address = $phone_number = $email_address = '';

if($GLOBAL_USERID > 0) {
    $result = $conn->query('SELECT * FROM User where userid=' . (int)$GLOBAL_USERID);
    if ($result->num_rows > 0) {
	     boka_lokal($conn, (int)$GLOBAL_USERID, $lokalkod);
    }
    exit;
}

header("Location: registrera.php?action=boka_lokal&lokalkod=$lokalkod");
exit;
