<?php
require("includes/open_database.php");

set_time_limit(7200);
ini_set('max_execution_time',7200);


if (!$isadmin) {
	header("Location: /");
	exit;
}

require("includes/header.php");

echo '<main class="row">  <div class="col-md-8 offset-md-2">';


if(isset($_GET['done']) && $_GET['done'] == 1) {
	echo '<h1>Klart!</h1>';
}

//$result = $conn->query("SELECT concat(namn, ' ', efternamn) namn, concat(adress, ' ', postadress) address, mail, telefon FROM User WHERE UserID IN (select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod  = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID GROUP BY kommunansvarig.UserID);");
//$result = $conn->query("select kommunansvarig.UserID from Booking LEFT JOIN vallokal ON Booking.LokalID = vallokal.LokalKod LEFT JOIN Kommun ON vallokal.KommunKod = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig ON Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID;");

//$result = $conn->query("select KommunKod, LanKod from Booking LEFT JOIN vallokal ON LokalID = LokalKod WHERE Booking.AntalR = 0 group by KommunKod, LanKod;");


//$result = $conn->query("select DISTINCT Booking.UserID from Booking LEFT JOIN vallokal on Booking.LokalID = vallokal.LokalKod  LEFT JOIN Kommun on vallokal.KommunKod = Kommun.KommunID AND vallokal.LanKod = Kommun.LänID LEFT JOIN kommunansvarig on Kommun.ID = kommunansvarig.kommunid WHERE Booking.AntalR = 0 and kommunansvarig.userid != Booking.UserID");
$numbers = [];
echo '<table class="table table-bordered table-hover">';

$r1 = $conn->query("select * from Booking where AntalR = 0;");
while($row1 = $r1->fetch_assoc()) {
	$r2 = $conn->query('select * from vallokal where LokalKod = "' .$conn->real_escape_string($row1['LokalID']). '";');
	$row2 = $r2->fetch_assoc();
	$r3 = $conn->query('select * from Kommun where LänID = "' .$conn->real_escape_string($row2['LanKod']). '" and KommunID = "' .$conn->real_escape_string($row2['KommunKod']). '";');
	$row3 = $r3->fetch_assoc();
	$r4 = $conn->query('select * from kommunansvarig where kommunid = "' .$conn->real_escape_string($row3['ID']). '";');
	$row4 = $r4->fetch_assoc();
	if ( $row4['userid'] == $row1['UserID']) {
		continue;
	}
	$r5 = $conn->query('select * from User where UserID = "' .$conn->real_escape_string($row1['UserID']). '";');
	$row = $r5->fetch_assoc();
	if (isset($numbers[$row['telefon']])) {
		continue;
	}
	$numbers[$row['telefon']] = $row['telefon'];
	echo '<tr><td>' . htmlspecialchars($row['namn'] . ' ' . $row['efternamn']) . '</td><td>' . htmlspecialchars($row['adress'] . ' '  . $row['postadress']) . '</td><td>' . htmlspecialchars($row['mail'])  . '</td><td>' . htmlspecialchars($row['telefon']) . '</td></tr>';
}



//$result = $conn->query("select * from User WHERE UserID IN (select DISTINCT UserID FROM (select Booking.UserID from Booking LEFT JOIN vallokal ON LokalID = vallokal.LokalKod left join Kommun ON vallokal.KommunKod = Kommun.KommunID and vallokal.LanKod = Kommun.LänID left join kommunansvarig on Kommun.ID = kommunansvarig.kommunid where kommunansvarig.userid IS NULL) t1);");

//while($row2 = $result->fetch_assoc()) {
//	$result2 = $conn->query("SELECT * FROM User WHERE UserID = $row2[UserID]");
//	$row = $result2->fetch_assoc(); // vad är 1, vad är 2? Levande variabler // arja
//	$result2 = $conn->query("SELECT * FROM Booking WHERE Booking.LokalID IN (select LokalKod from Kommun RIGHT JOIN vallokal ON KommunID = KommunKod AND LänID = LanKod WHERE Kommun.ID = $row[kommunid]) and Booking.UserID != $row[userid] AND Booking.AntalR = 0;");
//	while($row2 = $result2->fetch_assoc()) {
//		$result3 = $conn->query("SELECT * FROM User WHERE UserID = $row[userid]");
//		while($row3 = $result3->fetch_assoc()) {
//		}
//	}
//}

echo '</table>';

echo 'Totalt kommer ' . count($numbers) . ' SMS gå ut. ';
echo '<a href="smsa_aktivister_valsedlar.php?send=1">Skicka</a>';

echo '</div></main>';

if(isset($_GET['send']) && $_GET['send'] == 1) {
	foreach($numbers as $i => $number) {
//		file_put_contents($log, "$i: $number\n", FILE_APPEND);
		echo "$i: $number<br>";
		$sms = array(
		  "from" => "PP",   /* Can be up to 11 alphanumeric characters */
		  "to" => $number,  /* The mobile number you want to send to */
		  "message" => "Piratpartiet: Hej! Du har skrivit upp dig på att lägga ut valsedlar i förtidsröstnings- och/eller val-lokaler. Om du inte fått valsedlar än, ta kontakt med kommunansvarig. Kontaktuppgifter hittar du på https://valsedel.piratpartiet.se/mina_lokaler.php#valsedlar Om du inte får kontakt med kommunansvarig utan vill att vi postar valsedlar från Stockholm, hör av dig snarast till johan.karlsson@piratpartiet.se",
		);
		sleep(1);
		$res = sendSMS($sms);
		echo $res;
		flush();
	}
//	header("Location: smsa_kommunansvariga.php?done=1");

}

exit;
