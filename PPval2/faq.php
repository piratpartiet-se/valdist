<?php
require("includes/open_database.php");


require('includes/header.php');
?>
<main class='row'>
  <div class='col-md-8 offset-md-2'>

<h3>Hjälp oss få ut valsedlarna!</h3>
<p>Riksdagspartierna får sina valsedlar utlagda för sig i röst- och val-lokaler. Men Piratpartiet behöver själva lägga ut valsedlar. Och till skillnad från riksdagspartierna så har vi inga arvoderade som kan lösa det, utan det är upp till våra enskilda medlemmar att se till att valsedlarna kommer ut.</p>

<p>Vi har byggt en hemsida <a href='https://valsedel.piratpartiet.se'>https://valsedel.piratpartiet.se</a> för att hjälpa oss att koordinera arbetet. Vad du behöver göra nu <strong>(gärna idag)</strong> är att gå in och <strong>välja vilka vallokaler du kan hjälpa till med</strong>.
Så här går det till:

<ol>
<li>Gå in på <a href='https://valsedel.piratpartiet.se'>https://valsedel.piratpartiet.se</a></li>
<li>Välj ditt län och sedan kommun i listan</li>
<li>Du ser en lista över vilka röstlokaler som finns. Välj vilka du kan hjälpa till med. </li>
<li>Fyll i dina kontaktuppgifter </li>
<li>Du får kontaktuppgifter till personen som har valsedlar i din kommun. Prata med hen för att få valsedlar. Om det inte finns någon person så erbjuds du att vara mottagare. Om du har möjlighet, tacka gärna ja till det!</li>
<li>Så snart som möjligt efter att röstlokalerna öppnat, gå dit och lämna valsedlarna. </li></ol>

<h3>Vanliga frågor</h3>
<p><strong>När behöver valsedlarna läggas ut?</strong> I vallokaler så behöver valsedlarna läggas ut den 11:e September. Vallokalerna öppnar en halvtimme innan röstningen börjar, och såklart är det bra om valsedlarna är ute i alla vallokaler redan innan röstningen börjar, men det är mycket bättre du skriver upp dig på flera vallokaler och valsedlarna kommer ut lite senare än att valsedlarna inte kommer ut alls.</p>
<p><strong>Vad är skillnaden på röstlokal och vallokal?</strong><br>(Förtids)röstlokaler är öppna den 24:e Augusti till 10:e September. Vallokaler är endast öppna den 11:e September.</p>
<p><strong>Hur får jag tag på valsedlar att lägga ut?</strong><br>När du valt vilka lokaler du kan hjälpa till med så får du kontaktuppgifter till kommunansvarig. Hen kan ge dig valsedlar.</p>
<p><strong>Hur många valsedlar ska jag lägga ut?</strong> Du ser hur många valsedlar som ska lämnas i varje lokal under <a href='mina_lokaler.php#F'>Mina ansvarsområden</a>. Tips: en bunt med 100 valsedlar i är 1 centimeter tjock.</p>
<p><strong>Vad innebär det att vara kommunansvarig?</strong><br>Du kommer få en låda med valsedlar skickad till dig. OM det finns andra aktivister i kommunen som skrivit upp sig på att lägga ut valsedlar i några lokaler så får du kontaktuppgifter till dom. Kontakta ev aktivister för att lämna över valsedlar.<br>Även om det uppskattas ifall du försöker täcka så många lokaler som möjligt så finns det absolut inget sådant krav på de kommunansvariga. <u>Det är alltid bättre att du skriver upp dig på att vara kommunansvarig än att det inte finns någon. Alternativet är att valsedlarna ligger kvar i en källarlokal i Stockholm, och det är ju alltid sämre.</u></p>
<p><strong>Hur många valsedlar får jag som kommunansvarig skickat till mig?</strong> Det beror på hur stor din kommun är. I medelstora kommuner går det åt ungefär så många valsedlar som får plats i en skokartong. Överblivna valsedlar sorteras tillsammans med tidningspapper eller som brännbart.</p>
<p><strong>Hur gör man när man lägger ut valsedlarna?</strong><br>När du bokat in dig på en eller flera röstlokaler så ser du under <a href='mina_lokaler.php#F'>Mina ansvarsområden</a> hur många valsedlar du ska ta med till lokalen. Gå dit och säg till en röstmottagare att du ska lämna valsedlar. Röstmottagaren tar emot valsedlarna och lägger dom bland de andra valsedlarna.</p>

  </div>
</main>
