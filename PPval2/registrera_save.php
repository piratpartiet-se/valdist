<?php
require("includes/open_database.php");


$lokalkod = $conn->real_escape_string(isset($_POST["lokalkod"]) ? $_POST["lokalkod"] : '');
$kommun = $conn->real_escape_string(isset($_POST["kommun"]) ? $_POST["kommun"] : '');
$lan = $conn->real_escape_string(isset($_POST["lan"]) ? $_POST["lan"] : '');
$action = $conn->real_escape_string(isset($_POST["action"]) ? $_POST["action"] : '');

if ( $lokalkod !== '' ) {
	$lokal_data = get_lokal($conn, $lokalkod);
	$lan = $lokal_data['LanKod'];
	$kommun = $lokal_data['KommunKod'];
}

$first_name = $conn->real_escape_string($_POST["first_name"]);
$last_name = $conn->real_escape_string($_POST["last_name"]);
$street_adress = $conn->real_escape_string($_POST["street_address"]);
$post_address = $conn->real_escape_string($_POST["post_address"]);
$phone_number = $conn->real_escape_string($_POST["phone_number"]);
$email_address = $conn->real_escape_string($_POST["email_address"]);
$password = $conn->real_escape_string($_POST["password"]);
$password_confirmation = $conn->real_escape_string($_POST["password_confirmation"]);

if ($password !== $password_confirmation){
	exit();
}
$password_hash = password_hash($password, PASSWORD_DEFAULT);

// Add user if not exist
$result = $conn->query('SELECT * FROM User where mail="' .  $email_address . '"');
if ($result->num_rows == 0) {
    $conn->query('INSERT INTO User (mail, namn, efternamn, adress, postadress, telefon, `not`, Passwd) VALUES ("' . $email_address . '","' . $first_name . '","' . $last_name . '","' . $street_adress . '","' . $post_address . '","' . $phone_number . '","","'. $password_hash . '")');
}

login($conn, $email_address, $password);

if ( $action == 'kommunansvarig') {
	boka_kommunansvarig($conn, $GLOBAL_USERID, $lan, $kommun);
} else if ( $action == 'boka_lokal') {
	boka_lokal($conn, $GLOBAL_USERID, $lokalkod);
} else if ( $action == 'valdagshjalte') {
	boka_valdagshjalte($conn, $GLOBAL_USERID, $lan, $kommun);
} else {
	header('Location: /');
}
